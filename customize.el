(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(mu4e-compose-switch t nil nil "Customized with use-package mu4e")
 '(org-fold-catch-invisible-edits 'show-and-error nil nil "Customized with use-package org")
 '(pdf-misc-print-program-executable "lpr" nil nil "Customized with use-package pdf-tools")
 '(safe-local-variable-values '((eval org-media-marks 1))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
