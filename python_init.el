;;; Python
;(autoload 'python-mode "python-mode" "Elpy mode for Python" t)
(defun python-doc ()
  (interactive)
  (setq-local helm-dash-docsets '("Python 3")))

(add-hook 'python-mode-hook 'python-doc)
(add-hook 'python-mode-hook 'elpy-mode)

(add-hook 'elpy-mode-hook 'flycheck-mode)
(eval-after-load 'elpy-mode
  	  '(define-key elpy-mode-map (kbd "¾") 'recompile))


(setq elpy-rpc-backend "jedi")
;; (setq elpy-shell-display-buffer-after-send t)
(setq elpy-shell-echo-input-lines-head 3)
(setq elpy-shell-echo-input-lines-tail 3)
;; (setq elpy-test-pytest-runner-command '("py.test-3"))

(setq python-shell-interpreter "ipython" ;
      ;; fall back to readline ipython. needs rlipython package and ipython6 both from pip
      ;; python-shell-interpreter-args "-i --TerminalIPythonApp.interactive_shell_class=rlipython.TerminalInteractiveShell")
      python-shell-interpreter-args "-i --simple-prompt") 
