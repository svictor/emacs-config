;;; WEB MODE:
(autoload 'web-mode "web-mode" "Web Mode for html and mixed files" t)
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.twig\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.inc$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))

;; Auto-complete in web-mode is context-aware
(setq web-mode-ac-sources-alist
      '(("css" . (ac-source-words-in-buffer ac-source-css-property))
        ("html" . (ac-source-words-in-buffer ac-source-abbrev))
        ("php" . (ac-source-words-in-buffer
                  ac-source-words-in-same-mode-buffers
                  ac-source-dictionary))))

;;; PHP MODE:
;;;; Basics
(require 'php-mode)
(setq php-manual-path "~/bin/php_manual/") ; Use local manual in php-mode
(setq php-mode-speedbar-open t) ; Always open speedbar in php

;; Enable autocompletion
(add-hook 'php-mode-hook
          '(lambda ()
             (require 'company-php)
             (company-mode t)
             (ac-php-core-eldoc-setup) ;; enable eldoc
             (make-local-variable 'company-backends)
             (add-to-list 'company-backends 'company-ac-php-backend)))

;;;; Print_r skeleton
(define-skeleton vic-pretty-printr
  "Insert a printr($variable) nicely <pre> formatted (for arrays)."
  nil
  
  \n >
  "echo \'<pre\>\'; print\_r( _ ); echo \'</pre\>\';"
  \n >
  )

(define-abbrev php-mode-abbrev-table "vpp" "" 'vic-pretty-printr)

;; Local Variables:
;; eval: (outshine-mode)
;; End:
