;; Bootstrap straight
(setq straight-use-package-by-default t) ; we must set this before loading straight
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; Use-package
(setq use-package-verbose t)
(require 'use-package)

(defvar vic/init-dir "~/.emacs.d/inits/" "Directory of my init files")

;;; Load general settings
(load-file (concat vic/init-dir "orgmode_init.el"))

;;; Load org-mode setup
(load-file (concat vic/init-dir "general_init.el"))

;;; Load multimedia setup
(load-file (concat vic/init-dir "multimedia_init.el"))

;;; Load reading and bibliography setup
(load-file (concat vic/init-dir "bib_init.el"))

;;; Load mail setup
(load-file (concat vic/init-dir "mail_init.el"))


;; (cd (expand-file-name "~/.emacs.d"))
;; (load-file (expand-file-name "python_init.el"))
;; (load-file (expand-file-name "web_init.el"))


(cd (expand-file-name "~"))

