;;; Filter agenda files by filetag.
;; Used for org-caldav
(defun vic-org-quirks-agenda-files-by-filetags (filetag)
  "Return subset of `org-agenda-files' which have FILETAG set as a #+filetag."
  (delq nil
	(mapcar (lambda (f)
		  (when (eq 1 (string-to-number
			       (shell-command-to-string
				(concat "grep -c \"#+FILETAGS: .*" filetag "\" " f))))
		    f))
		(org-agenda-files))))


;;; Log when activating a task
(defun vic-org-quirks-log-todo-next-creation-date (&rest ignore)
  "Log NEXT creation time in the property drawer under the key 'ACTIVATED' and  remove any scheduling information."
  (let ((sched (org-entry-get nil "SCHEDULED")))
    ;; remove scheduling information, unless reccurent schedule
    (when (and sched (not (string-match-p "+" sched)))
      (org-schedule '(4))))
  (when (and (string= (org-get-todo-state) "NEXT")
             (not (org-entry-get nil "ACTIVATED")))
    (org-entry-put nil "ACTIVATED" (format-time-string "[%Y-%m-%d]"))))
(add-hook 'org-after-todo-state-change-hook #'vic-org-quirks-log-todo-next-creation-date)

;; Allow to create links with completion from org-refile-targets
  (defun vic-org-quirks-id-complete-link (&optional arg)
    "Create an id: link using completion"
    (concat "id:"
            (org-id-get-with-outline-path-completion org-refile-targets)))
  (org-link-set-parameters "id"
                           :complete 'org-id-complete-link)

;;; Insert modified time property
(defun vic-org-quirks-set-modified-time ()
  "Set MODIFIED property on current heading using current time."
  (interactive)
   "Sets the MODIFIED property of an orgmode header to current time"
  (org-set-property "MODIFIED"
		    (format-time-string "[%Y-%m-%d %a %H:%M]")))

;;; Follow links in dired
;; Credits to https://stackoverflow.com/a/37008095
(defun vic-org-quirks-link-dired-jump ()
  "Open Dired for directory of file link at point."
  (interactive)
  (let ((el (org-element-lineage (org-element-context) '(link) t)))
    (unless (and el (equal (org-element-property :type el) "file"))
      (user-error "Not on file link"))
    (dired-jump nil
                (expand-file-name (org-element-property :path el)))))

;;; Bring all files linked from buffer under a common directory
(defun vic-org-quirks-hardlink-file (link dest)
  "If LINK points to an existing file, hardlink that file from DEST directory and change all references to it in current buffer."
  (let* ((longpath (org-element-property :path link))
	 ;; we might need to truncate elements at end of path.
	 ;; For instance "::" is used in org-media-marks
	 (path (if (string-match "::" longpath)
		   (nth 0 (split-string longpath "::"))
		 longpath))
	 (type (org-element-property :type link))
	 (target (file-relative-name (file-name-concat dest (file-name-nondirectory path))))
         (desc (and (nth 2 link)
		    (substring-no-properties (nth 2 link))))
	 (replace-count 0)
	 (info))
    ;; debug
    ;; (list type (nth 0 (split-string path "::")) target desc)
    (cond
     ((member type '("http" "https")) ;; Skip online links
      (setq info (format "Skipping www link to %s." path)))
     ((not (file-exists-p path))
      (goto-char (org-element-property :begin link))
      (org-show-context)
      (user-error "File %s doesn't exist!" path))
     ((file-exists-p target)
      (setq info (format "Target %s already exists, skipping." target)))
     (t 
      (dired-hardlink path target)
      ;; replace occurrences in text
      (save-excursion
	(goto-char (point-min))
	;; handle possible #+LINK: declarations
	(while (search-forward (file-name-concat (file-name-directory path) "%s") nil t)
	  (replace-match (file-relative-name (file-name-concat dest "%s")))
	  (1+ replace-count))
	;; handle the actual links
	(goto-char (point-min))
	(while (search-forward path nil t)
	  (replace-match target nil t)
	  ;; unless we have "file:" at the beginning of link, insert
	  ;; "./" to tell org that this is an actually relative link
	  (goto-char (- (match-beginning 0) (length "file:")))
	  (unless (looking-at "file:")
	    (goto-char (match-beginning 0))
	    (insert "./"))
	  (1+ replace-count)))
      (setq info (format "Link %s from %s and replace %s references in buffer" path target replace-count))))
        ;; Return info
    info))


(defun vic-org-quirks-hardlink-files ()
  "Ask for a location and hardlink all file links in current buffer from that location, changing the link references accordingly. See ‘vic-org-quirks-hardlink-file’."
  (interactive)
  (let ((dest (read-directory-name "Directory to hardlink from: "))
	(info ""))
    (save-excursion
      (goto-char (point-min))
      (org-element-map (org-element-parse-buffer) 'link
	(lambda (link) (setq info (concat info "\n" (vic-org-quirks-hardlink-file link dest)))))
      (message info))))


;;; Move directory to archive
(defun vic-org-quirks-attach-move-dir-to-archive ()
  "Ask me to move any attachments of current subtree to the archive folder.
If archive location is not a folder (archiving to same file), do nothing."
  (let* ((org-attach-use-inheritance nil)
	 (attach-dir (org-attach-dir))
	 (attach-dir-relative (when attach-dir
				(directory-file-name
				 (file-relative-name attach-dir
						     (file-name-directory (or default-directory
									      buffer-file-name))))))
	 (archive-location (org-archive--compute-location (or (org-entry-get nil "ARCHIVE" 'inherit)
							      org-archive-location)))
	 (archive-file (car archive-location))
	 ;; the directory of the archive file
	 (archive-dir (file-name-directory archive-file))
	 ;; the directory where attachments should go
	 (archive-attach-dir (when attach-dir-relative
			       (concat archive-dir attach-dir-relative))))
    
    (when (and attach-dir-relative archive-attach-dir)
      (when (yes-or-no-p
	     (format "Move attachment directory %s to %s?"
		     attach-dir-relative archive-attach-dir))
	;; ensure that all the parents of archive dir exist
	(let ((parent (file-name-directory archive-attach-dir)))
	  (unless (file-exists-p parent)
	    (make-directory parent t)))
	(rename-file attach-dir-relative archive-attach-dir)
	(org-delete-property "DIR") 
	;; try to only run this when archiving
	(let ((archive-buffer (find-buffer-visiting archive-file)))
	  (when archive-buffer
	      (with-current-buffer archive-buffer
		(org-entry-put nil "DIR" (if org-attach-dir-relative
					     attach-dir-relative
					   (expand-file-name attach-dir-relative)))
		(message "Added attachment dir property to heading \"%s\" in file %s " (nth 4 (org-heading-components)) archive-file))))))))

(provide 'vic-org-quirks)
