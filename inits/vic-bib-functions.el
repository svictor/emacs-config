;; Interact with my Boox reader

(defvar vic/bib-pdfs-sync-dir "~/bib/toread/")

(defun vic/citar-push-files-to-sync (citekey)
  (let ((files (delete-dups (gethash citekey (citar-get-files citekey)))))
    (cl-loop for file in files
	     do (when (and (string= (file-name-base file) citekey)
			   (yes-or-no-p (format "Copy file %s to %s?" file vic/bib-pdfs-sync-dir)))
		  (vic/bib-push-file-to-sync file))))) 

(defun vic/bib-push-file-to-sync (file)
  "Push FILE to the sync folder defined in ‘vic/bib-pdfs-sync-dir’."
  (let ((sync-file (concat
		   (file-name-as-directory vic/bib-pdfs-sync-dir)
		   (file-name-nondirectory file))))
    (if (file-exists-p sync-file)
	(user-error "File already exists: %s" sync-file)
      (copy-file file sync-file))))

(defun vic/citar-pull-files-from-sync (citekey)
  "Copy the files associated with CITEKEY which exist in ‘vic/bib-pdfs-sync-dir’."
  (let ((files (delete-dups (gethash citekey (citar-get-files citekey)))))
    (cl-loop for file in files	     
	     do (vic/citar-pull-file-from-sync file)))
    ;; popup note
    (let ((hasnotesp (citar-has-notes))) ;; weird way of testing but see docstring of citar-has-notes
      (if (funcall hasnotesp citekey)
	  (setq messages (concat messages
				 (format "There is already a note for %s" citekey)))
	(citar-create-note citekey)))  
    (org-end-of-meta-data t)
    ;; Insert AI notes from reader if found
    (vic/citar-pull-notes-from-sync))


(defun vic/citar-pull-notes-from-sync (citekey)
  "Get notes from sync folder and insert them in current document"
  (let* ((txt-notes (concat citekey "_note_AI.txt"))
	   (txt-notes-sync-dir (concat "~/sync/boox_notes/" citekey "_note/"))
	   (ai-notes (concat txt-notes-sync-dir txt-notes)))
      (when (file-exists-p ai-notes)
	(message "AI notes found. Copying them in as well.")
	(org-insert-subheading t)
	(insert "General notes")
	(newline-and-indent)
	(insert-file-contents ai-notes)
	(move-file-to-trash ai-notes))))

(defun vic/citar-pull-file-from-sync (file)
  "Import FILE from my ereader to the local storage."
  ;; Unmounts reader on mtp, to start clean
  (let* ((key (file-name-base file))
	 (filename (file-name-nondirectory file))
	 (sync-dir (file-name-as-directory vic/bib-pdfs-sync-dir))
	 (sync-pdf (concat sync-dir filename))
	 (tmp-dir (concat temporary-file-directory key "/"))
	 (tmp-pdf (concat tmp-dir filename))	 
	 (mtp "mtp://Android_Android_89179b9b/")
	 (local-pdf file)
	 (mtp-connected)
	 (messages (string)))

    ;; remount mtp connection
    (shell-command (concat "gio mount -u " mtp))
    (setq mtp-connected (equal 0 (shell-command (concat "gio mount " mtp))))

    ;; Get file through mtp if available
    (when mtp-connected
      (setq messages
	    (concat messages  "Reader connected through MTP.\n"))
      ;; Copy file to tmp-dir
      (unless (file-exists-p tmp-dir) (make-directory tmp-dir))
      (shell-command (concat "gio copy " mtp
			     "Internal%20shared%20storage/bib/toread/" filename " "
			     tmp-dir))
      ;; Copy auxiliary notes to notes sync dir. Overwrites whatever
      ;; note might be there for this key, but that's fine since
      ;; these notes are only modified on the reader.
      (shell-command (concat "gio copy " mtp
			     "Internal%20shared%20storage/note/" key "_note/"
			     txt-notes " "
			     txt-notes-sync-dir)))

    ;; Compare files and import
    (let ((tmp-md5 (when (file-exists-p tmp-pdf)
		     (car (split-string (shell-command-to-string (format "md5sum %s" tmp-pdf))))))
	  (local-md5 (when (file-exists-p local-pdf)
		       (car (split-string (shell-command-to-string (format "md5sum %s" local-pdf))))))
	  (sync-md5 (when (file-exists-p sync-pdf)
		      (car (split-string (shell-command-to-string (format "md5sum %s" sync-pdf)))))))

      ;; Basic checks
      (unless (file-exists-p sync-pdf)
	(setq messages
	      (concat messages  (format "File was not properly synced to %s." sync-pdf)))
	(user-error messages))
      (unless (file-exists-p local-pdf)
	(setq messages
	      (concat messages  (format "No local file %s" file)))
	(user-error messages))

      ;; What's the status, Charlie?
      (setq messages 
	    (concat messages  (format "Importing file for key %s\n" key)))
      (setq messages 
	    (concat messages  (format "Local is %s with hash %s\n" local-pdf local-md5)))
      (setq messages 
	    (concat messages  (format "Sync is %s with hash %s\n" sync-pdf sync-md5)))
      (when (file-exists-p tmp-pdf)
	(setq messages 
	      (concat messages  (format "Mtp is %s with hash %s\n" tmp-pdf tmp-md5))))
      
      ;; What shall we do?
      (cond
       ((and (equal sync-md5 local-md5)
	     (if tmp-md5 (equal tmp-md5 local-md5) t))
	(setq messages 
	      (concat messages  (format "Local and distant file look the same. Trash distant %s?" sync-pdf)))
	(when (yes-or-no-p messages )
	  ;; Move sync file to trash. Tmp file will disappear by itself.
	  (move-file-to-trash sync-pdf)))
       ((time-less-p (file-attribute-modification-time (file-attributes sync-pdf))
		     (file-attribute-modification-time (file-attributes local-pdf)))
	(setq messages 
	      (concat messages  (format "%s is newer than %s! Aborting." local-pdf sync-pdf)))
	(user-error messages ))
       ((and tmp-md5 
	     (not (equal sync-md5 tmp-md5))
	     (not (equal sync-md5 local-md5))
	     (not (equal tmp-md5 local-md5)))
	(find-file-other-window tmp-pdf)
	(find-file-other-window sync-pdf)
	(setq messages 
	      (concat messages  (format "Both %s and %s were modified. Merge or remove one of them." tmp-pdf sync-pdf)))
	(user-error messages ))
       ((or (not (equal sync-md5 local-md5)) ; 1st cond! Don't succeed on tmp-md5 non-nil.
	    (not (equal tmp-md5 local-md5)))
	(setq messages 
	      (format "Trash %s and replace it with %s?" local-pdf (if tmp-md5 tmp-pdf sync-pdf)))
	(when (yes-or-no-p messages)
	  (move-file-to-trash local-pdf)
	  ;; (rename-file local-pdf (concat local-pdf ".bak"))
	  (copy-file (if tmp-md5 tmp-pdf sync-pdf) local-pdf)
	  ;; Cleanup synced file, to clean it on reader too.
	  (move-file-to-trash sync-pdf))))
       (t
	(user-error (format "Couldn't find out how to import attachment of %s!\n" key))))))


(defun vic/boox-cleanup-notes ()
  "Cleanup notes taken on my boox reader. Convert them to (my) org syntax."
  (interactive)
  (let* ((time-rx "^Time：.*")
	 (quote-rx "【Original Text】\\(.*\\)")
	 (annotation-rx "【Annotations】\\(.*\\)")
	 (page-number-rx "【Page Number】\\(.*\\)")
	 (separator-rx "-------------------")
	 (global-rx (mapconcat (lambda (x) (concat x "\n"))
			       (list time-rx quote-rx annotation-rx page-number-rx separator-rx) "")))
    (goto-char (point-min))
    (while (re-search-forward global-rx nil t)
      (let ((origtext (buffer-substring-no-properties (match-beginning 1) (match-end 1)))
	    (comment (buffer-substring-no-properties (match-beginning 2) (match-end 2)))
	    (page (buffer-substring-no-properties (match-beginning 3) (match-end 3))))
	(delete-region (match-beginning 0) (match-end 0))
	(when (< 0 (length origtext))
	  (insert (format "#+begin_quote\n%s (p. %s)\n#+end_quote\n" origtext page))
	  (setq page nil))
	(when (< 0 (length comment))
	  (insert (format "#+begin_my\n%s" comment))
	  ;; put page number here in case of autonomous comment with no quote
	  (when page (insert (format "(p. %s)" page)))
	  (insert (format "\n#+end_my\n")))))))


;;; Other functions
(defun vic/pdf-strip-annots (&optional filename)
  "Call Vpdfstripannots script to strip pdf FILENAME of its annotations and output result in /tmp/filename_0.pdf"
  (interactive)
  (let ((file (or filename
		  (read-file-name "Choose a pdf file: "
				  (car bibtex-completion-library-path) nil nil nil
				  (lambda (fname)
				    (or (directory-name-p fname)
					(string-suffix-p ".pdf" fname t)))))))
    (shell-command (concat "/home/vic/.local/bin/vscripts/Vpdfstripannots " file))))

(defun vic/bib-pdf-file-name (citekey)
  "If CITEKEY.pdf exists, return that filename. "
  (let ((fname (file-name-concat (car citar-library-paths) (concat citekey ".pdf"))))
    (when (file-exists-p fname) fname)))

(provide 'vic-bib-functions)
