;;; BOOTSTRAP

 (use-package mu4e
   :load-path "/usr/share/emacs/site-lisp/elpa-src/mu4e-1.12.8/"
  :straight (:local-repo "/usr/share/emacs/site-lisp/elpa/mu4e-1.12.8/"
             :type built-in)
   :demand t
   :custom
   (mail-user-agent 'mu4e-user-agent) ; Make it default mail client for C-x m
   (mu4e-context-policy 'pick-first) ; start with the first (default) context;
   (mu4e-compose-signature-auto-include nil) ; No auto signature, use C-c C-w to include one
   (mu4e-compose-format-flowed nil) 	; don’t compose format-flowed
					; messages. Adds fill-column
					; line breaks, random
					; rendering on various clients
   (fill-flowed-encode-column fill-column) ; override the default 66,
					   ; I prefer larger cols of
					   ; 72
   (mu4e-view-use-gnus t)              ; use gnus to display messages (considered experimental)
   (mu4e-use-fancy-chars t)
   (mu4e-compose-in-new-frame t)
   (mu4e-view-show-addresses t)
   (mu4e-index-update-error-continue t)
   (mu4e-index-update-error-warning t)
   (mu4e-index-lazy-check t)
   (mu4e-hide-index-messages t)
   (mu4e-view-show-images t)
   (mu4e-get-mail-command (expand-file-name "~/.local/bin/vscripts/recevoir_mails"))
   ;; the headers to show in the headers list -- a pair of a field
   ;; and its width, with `nil' meaning 'unlimited'
   ;; (better only use that for the last field.
   (mu4e-headers-fields
    '( (:human-date . 12)
       (:flags . 6)
       (:mailing-list . 10)
       (:from-or-to . 22)
       (:thread-subject . nil)))
  ;; Limit address auto-complete
  (mu4e-compose-complete-only-after "2020-01-01")
  (mu4e-contact-process-function 'vic/make-contact-blacklist-regexp)
  
  ;; folders
  (mu4e-maildir "~/.mail")
  (mu4e-attachment-dir "~/Downloads")
  (mu4e-sent-folder "/Sent")
  (mu4e-refile-folder "/archives")
  (mu4e-drafts-folder "/Drafts")
  (mu4e-trash-folder "/Trash")

  ;; Bookmarks
  (mu4e-bookmarks
	'(( :name "For me today"
	    :query "date:today..now AND NOT flag:trashed AND NOT from:LISTSERV@JISCMAIL.AC.UK AND NOT list:debian-user.lists.debian.org AND NOT list:mu-discuss.googlegroups.com AND NOT list:emms-help.gnu.org AND NOT list:yoshimi.freelists.org AND NOT list:linux-audio-user.lists.linuxaudio.org AND NOT list:debian-security-announce.lists.debian.org" 
	    :key ?t)
	  ( :name "CREM today"
	    :query "date:today..now AND (contact:crem.lesc@cnrs.fr OR contact:coarchcrem@services.cnrs.fr) "
	    :key ?c)
	  ( :name "CREM general"
	    :query "contact:crem.lesc@cnrs.fr OR contact:coarchcrem@services.cnrs.fr"
	    :key ?C)
	  ( :name "Lists today"
	    :query "date:today..now AND flag:list AND NOT flag:trashed AND NOT m:/archives/ AND NOT from:LISTSERV@JISCMAIL.AC.UK"
	    :key ?l)
	  ( :name "For me"
	    :query "(NOT flag:list AND NOT flag:trashed AND NOT from:LISTSERV@JISCMAIL.AC.UK AND NOT list:debian-user.lists.debian.org AND NOT list:mu-discuss.googlegroups.com AND NOT list:emms-help.gnu.org AND NOT list:yoshimi.freelists.org AND NOT list:linux-audio-user.lists.linuxaudio.org AND NOT list:debian-security-announce.lists.debian.org"
	    :key ?v)
	  ( :name "Lists"
	    :query "from:LISTSERV@JISCMAIL.AC.UK OR (flag:list AND NOT flag:trashed)"
	    :key ?L
	    :hide t)
	  ( :name "Unread" :query "flag:unread AND NOT flag:trashed"
	    :key ?u
	    :hide t)
	  ( :name "Archived" :query "m:/archives/"
	    :key ?a
	    :hide t)
	  ( :name "Drafts"
	    :query "flag:draft"
	    :key ?d
	    :hide t)))

  ;; Shortcuts
  (mu4e-maildir-shortcuts
	'((:maildir "/svictor" :key ?v)
	  (:maildir "/victor"  :key ?i)
	  (:maildir "/vs"      :key ?s)
	  (:maildir "/cnrs"    :key ?c)))

    ;; Sending
  (sendmail-program (expand-file-name "~/.local/bin/vscripts/envoyer_mails")) ; symlink to the actual program
  (mail-specify-envelope-from t)
  ;; needed for debians message.el cf. README.Debian.gz 

  (mail-envelope-from 'header)

  :bind
  ("C-c m" . 'mu4e)
  (:map mu4e-main-mode-map ("q" . 'previous-buffer)) ;; rebind from mu4e-quit

  :config  
  (add-to-list 'auto-mode-alist '("\\.eml$" . mail-mode)) ;Open .eml files in mail-mode

  ;; Define contexts
  (setq mu4e-contexts
   `( ,(make-mu4e-context
	:name "svictor"
	:enter-func (lambda () (mu4e-message "Switch to the Svictor context"))
	;; leave-func not defined
	:match-func (lambda (msg)
		      (when msg
			(string-match-p "^/svictor" (mu4e-message-field msg :maildir))))
	:vars '(  ( user-mail-address . "svictor@svictor.net"  )
		  ( user-full-name . "Victor A. Stoichita" )
		  ( mu4e-refile-folder . "/archives/svictor" )))
	 
      ,(make-mu4e-context
	:name "prof"
	:enter-func (lambda () (mu4e-message "Switch to the CNRS context"))
	:match-func (lambda (msg)
		      (when msg
			(string-match-p "^/cnrs" (mu4e-message-field msg :maildir))))
	:vars '(  ( user-mail-address	. "victor.stoichita@cnrs.fr" )
		  ( user-full-name . "Victor A. Stoichita" )
		  ( mu4e-refile-folder . "/archives/cnrs" )))
	 ,(make-mu4e-context
	   :name "crem"
	   :enter-func (lambda () (mu4e-message "Switch to the CREM context"))
	   :match-func (lambda (msg)
			 (when msg
			   (string-match-p "^/crem" (mu4e-message-field msg :maildir))))
	   :vars '(  ( user-mail-address . "crem.lesc@cnrs.fr" )
		     ( user-full-name . "Centre de recherche en ethnomusicologie" )
		     ( mu4e-refile-folder . "/archives/crem" )))
	 
	 ,(make-mu4e-context
	   :name "listes"
	   :enter-func (lambda () (mu4e-message "Switch to Lists context"))
	   ;; leave-func not defined
	   :match-func (lambda (msg)
			 (when msg
			   (string-match-p "^/victor" (mu4e-message-field msg :maildir))))
	   :vars '(  ( user-mail-address . "victor@svictor.net"  )
		     ( user-full-name . "Victor A. Stoichita" )
		     ( mu4e-refile-folder . "/archives/victor" )))
	 
	 ,(make-mu4e-context
	   :name "rubbish"
	   :enter-func (lambda () (mu4e-message "Switch to vs context"))
	   ;; leave-func not defined
	   :match-func (lambda (msg)
			 (when msg
			   (string-match-p "^/vs" (mu4e-message-field msg :maildir))))
	   :vars '(  ( user-mail-address . "vs@svictor.net"  )
		     ( user-full-name . "Victor" )
		     ( mu4e-refile-folder . "/archives/vs" )))))
  
  (defvar vic/contact-blacklist-file "/home/vic/.mail/contact-blacklist" "List of blacklisted mail addresses")
  (defun vic/read-contact-blacklist ()
    "Return a list of blacklisted email addresses"
    (with-temp-buffer
      (insert-file-contents vic/contact-blacklist-file)
      (split-string (buffer-string) "\n" t)))
  
  (defun vic/make-contact-blacklist-regexp (address)
    "Check if address belongs to blacklist"
    ;; Combine blacklisted addresses into a regexp
    (let ((blacklist (mapconcat 'identity (vic/read-contact-blacklist) "\\|")))
      (if (string-match-p blacklist address)
	  nil
	address)))

  ;; SENDING:
  ;; From http://ionrock.org/emacs-email-and-mu.html
  ;; Choose account label to feed msmtp -a option based on From header
  ;; in Message buffer; This function must be added to
  ;; message-send-mail-hook for on-the-fly change of From address before
  ;; sending message since message-send-mail-hook is processed right
  ;; before sending message.
  (defun choose-msmtp-account ()
    (if (message-mail-p)
	(save-excursion
	  (let*
	      ((from (save-restriction
		       (message-narrow-to-headers)
		       (message-fetch-field "from")))
	       (account
		(cond
		 ((string-match "svictor@svictor.net" from) "svictor")
		 ((string-match "victor@svictor.net" from) "victor")
		 ((string-match "vs@svictor.net" from) "vs")
		 ((string-match "victor.stoichita@cnrs.fr" from) "cnrs")
		 ((string-match "crem.lesc@cnrs.fr" from) "crem")
		 (t (user-error "No account configured to send from %s" from)))))
	    (setq message-sendmail-extra-arguments (list '"-a" account))))))

  
  :hook
  (message-send-mail . choose-msmtp-account))
  
(use-package gnus
  :custom
  (gnus-dired-mail-mode 'mu4e-user-agent)
  (message-sendmail-f-is-evil nil)
  (message-prune-recipient-rules nil)
  (message-kill-buffer-on-exit t)	;don’t keep message buffers around
  (message-citation-line-function (quote message-insert-formatted-citation-line))
  (message-citation-line-format "Le %d %b %Y, %f a écrit :\n")
  (message-cite-reply-position 'above)
  (message-send-mail-function 'message-send-mail-with-sendmail)
  (message-sendmail-envelope-from 'header)
  (message-forward-as-mime t)

  :defer 30 ;; autoload after 30seconds of inactivity. Relying
	    ;; on dired-mode hook isn’t enough
  :config  
  ;; make the `gnus-dired-mail-buffers' function also work on
  ;;  message-mode derived modes, such as mu4e-compose-mode.
  ;; mark the file(s) in dired you would like to attach and press
  ;; C-c RET C-a, and you’ll be asked whether to attach them to an existing
  ;; message, or create a new one.
  (defun vic/gnus-dired-mail-buffers ()
    "Return a list of active message buffers."
    (let (buffers)
      (save-current-buffer
	(dolist (buffer (buffer-list t))
	  (set-buffer buffer)
	  (when (and (derived-mode-p 'message-mode)
		     (null message-sent-message-via))
	    (push (buffer-name buffer) buffers))))
      (nreverse buffers)))
  (advice-add 'gnus-dired-mail-buffers :override #'vic/gnus-dired-mail-buffers)

  :hook
  (dired-mode . turn-on-gnus-dired-mode)
  (message-mode-hook . (lambda ()
			 (auto-fill-mode)
			 (orgalist-mode)
			 (footnote-mode)
			 (flyspell-mode))))

;;; Orgalist
(use-package orgalist)

;;;; View  
;; Don’t display html if text is available
(with-eval-after-load "mm-decode"
       (add-to-list 'mm-discouraged-alternatives "text/html")
       (add-to-list 'mm-discouraged-alternatives "text/richtext"))

;;;; Htmlize
(use-package org-mime
  :bind
  (:map message-mode-map ("C-c M-o" . 'org-mime-htmlize))
  (:map org-mode-map ("C-c M-o" . 'org-mime-org-subtree-htmlize))
  ;; Disable some export defaults when composing email from org

  :custom
  (org-mime-export-options '(:section-numbers nil :with-author nil :with-toc nil))

  :config
  (defun vic/org-mime-tweak-html-output ()
    "Change some elements of html output from org-mime"
    (org-mime-change-element-style
     "blockquote" "border-left: 2px solid gray; padding-left: 4px;"))
  :hook
  ;; Offset block quotes in email bodies
  (org-mime-html . vic/org-mime-tweak-html-output))

(defun vic/message-check-htmlize-before-send ()
  "Check outgoing message for indications that it should perhaps be htmlized. A message might want to be htmlized when it holds org syntax. This function only checks for some elements of org syntax (those that I might want to include in an email)."
  (let* ((found-multipart (save-excursion
                            (save-restriction
			      (widen)
			      (goto-char (point-min))
			      (search-forward "<#multipart type=alternative>" nil t))))
	   (org-syntax (concat (mapconcat (lambda (re)
					    (format "\\(%s\\)\\|" re))
					  '(org-outline-regexp
					    org-link-any-re
					    org-property-re
					    org-element-citation-key-re
					    org-table-line-regexp))
			       ;; Rich text syntax * / _
			       "\\([ 	('\"{]\\|^\\)\\(\\([/|\\*|_]\\)\\([^ \t\n,\"']\\|[^ \t\n,\"'].*?\\(?:\n.*?\\)\\{0,1\\}[^ \t\n,\"']\\)[/|\\*|_]\\)\\([- \t.,:!?;'\")}\\]\\|$\\)"))
	   (found-org-syntax (save-excursion
			       (message-goto-body)
			       (re-search-forward org-syntax nil t))))
    (when (and found-org-syntax
	       (not found-multipart))
      (goto-char found-org-syntax)
      (when (not (y-or-n-p "It seems `org-mime-htmlize' is NOT called; send anyway? "))
	(setq quit-flag t)))))

(add-hook 'message-send-hook 'vic/message-check-htmlize-before-send)

;; Local Variables:
;; eval: (outshine-mode)
;; End:

