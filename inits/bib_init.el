;;; Pdf tools
(use-package pdf-tools
  :custom
  (pdf-view-display-size 'fit-page) ; open pdfs scaled to fit page
  (pdf-misc-print-programm "lpr")
  (pdf-view-midnight-colors '("#fdf4c1" . "#282828"))

    :config
  (pdf-loader-install) ; prepare emacs to load pdf-tools when opening
		       ; a pdf but not sooner
  ;; Functions to save to kill ring after annotating
  (defun vic/pdf-annot-add-highlight-markup-annotation-and-kill ()
    "Highlight and copy region to `kill-ring'."
    (interactive)
    (pdf-annot-add-highlight-markup-annotation (pdf-view-active-region nil))
    (pdf-view-kill-ring-save))
  
  (defun vic/pdf-annot-add-underline-markup-annotation-and-kill ()
    "Highlight and copy region to `kill-ring'."
    (interactive)
    (pdf-annot-add-underline-markup-annotation (pdf-view-active-region nil))
    (pdf-view-kill-ring-save))
  
  (defun vic/pdf-annot-add-squiggly-markup-annotation-and-kill ()
    "Highlight and copy region to `kill-ring'."
    (interactive)
    (pdf-annot-add-squiggly-markup-annotation (pdf-view-active-region nil))
    (pdf-view-kill-ring-save))
  
  (defun vic/pdf-annot-toggle-activate-created-annotations ()
    "Toggle automatic activation of new annotations"
    (interactive)
    (when (string= major-mode "pdf-view-mode")
      (if pdf-annot-activate-created-annotations
	  (setq-local pdf-annot-activate-created-annotations nil)
	(setq-local pdf-annot-activate-created-annotations t))
      (message (format "Activate created annotations %s" pdf-annot-activate-created-annotations))))
  
  :bind
  (:map pdf-view-mode-map
	("C-s" . 'isearch-forward) ; use normal isearch
	("\"" . 'vic/pdf-annot-add-underline-markup-annotation-and-kill)
	("«" . 'vic/pdf-annot-add-highlight-markup-annotation-and-kill)
	("»" . 'vic/pdf-annot-add-squiggly-markup-annotation-and-kill)
	("(" . 'pdf-annot-add-text-annotation)
	("*" . 'vic/pdf-annot-toggle-activate-created-annotations)))

(use-package org-pdftools
  :hook (org-mode . org-pdftools-setup-link))

;; Doesn’t seem to work for now.
;; Problem when syncing pdf from note. Try later !
;; (use-package org-noter-pdftools
;;   :after org-noter
;;   :config
;;   ;; Add a function to ensure precise note is inserted
;;   (defun org-noter-pdftools-insert-precise-note (&optional toggle-no-questions)
;;     (interactive "P")
;;     (org-noter--with-valid-session
;;      (let ((org-noter-insert-note-no-questions (if toggle-no-questions
;;                                                    (not org-noter-insert-note-no-questions)
;;                                                  org-noter-insert-note-no-questions))
;;            (org-pdftools-use-isearch-link t)
;;            (org-pdftools-use-freepointer-annot t))
;;        (org-noter-insert-note (org-noter--get-precise-info)))))

;;   ;; fix https://github.com/weirdNox/org-noter/pull/93/commits/f8349ae7575e599f375de1be6be2d0d5de4e6cbf
;;   (defun org-noter-set-start-location (&optional arg)
;;     "When opening a session with this document, go to the current location.
;; With a prefix ARG, remove start location."
;;     (interactive "P")
;;     (org-noter--with-valid-session
;;      (let ((inhibit-read-only t)
;;            (ast (org-noter--parse-root))
;;            (location (org-noter--doc-approx-location (when (called-interactively-p 'any) 'interactive))))
;;        (with-current-buffer (org-noter--session-notes-buffer session)
;;          (org-with-wide-buffer
;;           (goto-char (org-element-property :begin ast))
;;           (if arg
;;               (org-entry-delete nil org-noter-property-note-location)
;;             (org-entry-put nil org-noter-property-note-location
;;                            (org-noter--pretty-print-location location))))))))
;;   (with-eval-after-load 'pdf-annot
;;     (add-hook 'pdf-annot-activate-handler-functions #'org-noter-pdftools-jump-to-note)))



;;; Org-noter
(use-package org-noter
  :custom
  (org-noter-hide-other nil)
  (org-noter-notes-search-path '("~/bib/"))
  (org-noter-doc-property-in-notes nil) 
  (org-noter-default-notes-file-names "~/bib/reading_notes.org") ; not used, in practice
  (org-noter-auto-save-last-location nil)
  

  :bind
  (:map org-mode-map
	("C-c (" . 'org-noter))

  :config
  ;; Return to notes window after sync (otherwise stays on doc window)
  (advice-add 'org-noter-sync-current-note
	      :after (lambda () (select-window (org-noter--get-notes-window))))
  (advice-add 'org-noter-sync-prev-note
	      :after (lambda () (select-window (org-noter--get-notes-window))))
  (advice-add 'org-noter-sync-next-note
	      :after (lambda () (select-window (org-noter--get-notes-window))))
  ;; Try to shorten buffer name?
  :hook
  (org-noter-doc .  vic/org-noter-mode-line)
  ;; Don’t protect the first headline’s properties
  (org-noter-notes . (lambda () (setq-local inhibit-read-only t))))


;;; Org-roam
(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory (file-truename "~/notes"))
  (org-roam-capture-templates '(("i" "idea" plain "%?" :target
				 (file+head "ideas/%<%y%m%d>-${slug}.org" "#+title: ${title}")
				 :unnarrowed t)	       
				("r" "read" plain "* ${title}\n :PROPERTIES:\n :ID: %(org-id-new)\n :END:\n\n%?" :target 
				 (file "references/read.org"))
				("w" "watch" plain "* ${title}\n :PROPERTIES:\n :ID: %(org-id-new)\n :END:\n\n%?" :target 
				 (file "references/watch.org"))
				("s" "liSten" plain "* ${title}\n :PROPERTIES:\n :ID: %(org-id-new)\n :END:\n\n%?" :target 
				 (file "references/listen.org"))
				("p" "poetic" plain "* ${title}\n :PROPERTIES:\n :ID: %(org-id-new)\n :END:\n\n%?" :target 
				 (file "references/poetic.org"))
				("c" "confinaire" plain "%?" :target
				 (file+head "confinaires/%<%y%m%d>-${slug}.org"
					    "#+filetags: confinaires ${event}\n#+title: ${title}")
				 :unnarrowed t)
				("t" "tag" plain "%?" :target
				 (file+head "indexes/${slug}.org" "#+title: ${title}")
				 :unnarrowed t)
				("l" "literature note" plain
				 "%?"
				 :if-new
				 (file+head
				  "bib/${citar-citekey}.org"
				  "#+title: ${note-title}.\n#+created: %U\n\n* ${citar-title}\n:PROPERTIES:\n:NOTER_DOCUMENT: %(vic/bib-pdf-file-name \"${citar-citekey}\") \n:END:\n\n")
				 :unnarrowed t)))

  (org-roam-dailies-capture-templates
      '(("d" "default" entry
         "* %?"
         :target (file+head "%<%Y-%m-%d>.org.gpg"
                            "#+title: %<%Y-%m-%d>\n"))))
  (org-roam-capture-ref-templates '(("r" "ref" plain "%?" :target
				     (file+head "%<%y%m%d>-${slug}.org" "#+title: ${title}")
				     :unnarrowed t)))
  (org-roam-dailies-directory "~/.pebbles")
  :bind (("C-c n l" . org-roam-buffer-toggle)
	 
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
	 ("C-c n p" . org-roam-dailies-goto-today )
	 ("C-c n r" . vic/org-roam-rg-search))
  :config
  ;; Ripgrep for my notes files
  ;; From https://org-roam.discourse.group/t/using-consult-ripgrep-with-org-roam-for-searching-notes/1226
  (defun vic/org-roam-rg-search ()
  "Search org-roam directory using consult-ripgrep. With live-preview."
  (interactive)
  (let ((consult-ripgrep-command "rg --null --ignore-case --type org --line-buffered --color=always --max-columns=500 --no-heading --line-number . -e ARG OPTS"))
    (consult-ripgrep org-roam-directory)))

  ;; Show node hierarchy
  ;; from https://github.com/org-roam/org-roam/wiki/User-contributed-Tricks#showing-node-hierarchy
  (cl-defmethod org-roam-node-hierarchy ((node org-roam-node))
  (let ((level (org-roam-node-level node)))
    (concat
     (when (> level 0) (concat (org-roam-node-file-title node) " > "))
     (when (> level 1) (concat (string-join (org-roam-node-olp node) " > ") " > "))
     (org-roam-node-title node))))

  ;; ensure we are in a proper node when linking to another
  (advice-add 'org-roam-node-insert :after 'org-id-get-create)
  
  (setq org-roam-node-display-template (concat "${hierarchy:*} " (propertize "${tags:10}" 'face 'org-tag)))
  
  (org-roam-db-autosync-mode)
  ;; If using org-roam-protocol
  (require 'org-roam-protocol))

(use-package citar-org-roam
:after (citar org-roam)
:config (citar-org-roam-mode)
:custom
(citar-org-roam-note-title-template "${author editor} (${year date}) :: ${title}")
(citar-org-roam-capture-template-key "l"))

(use-package org-roam-ui
    :after org-roam
;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
;;         a hookable mode anymore, you're advised to pick something yourself
;;         if you don't care about startup time, use
;;  :hook (after-init . org-roam-ui-mode)
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t))

;;; Org-to-org
(use-package org2org
  :straight (:local-repo "org2org")
  :custom
  (o2o-string-replacements '(("^@T[[:space:]]*$" . "TODO")
			     ("^@REL " . "REL ")
			     ("^CRIT[:space:]*$" . "TAG CRIT\n")			     
			     ("^@REF" . "REF")
			     ;; Rely on o2o-retitle-header-from-next-quote
			     ("^@*[[:space:]]*$" . "")))
  (o2o-commands '(;; Headings to remove based on title
		  ("Link on page "
		   (o2o-remove-header) (org-at-heading-p))
		  ;; Transforming headers into blocks
		  ("Contents$"
		   (o2o-command-replace-header-with-block "quote") (org-at-heading-p))
		  ("Comment$"
		   (o2o-command-replace-header-with-block "my") (org-at-heading-p))
		  ;; Removing blocks based on comment annotation
		  ("^SKIP$"
		   (o2o-remove-header) (org-in-block-p ("my")))
		  ;; Combining blocks			
		  ("\\(?:^@[[:space:]]*C$\\|^CONT[[:space:]]*$\\)"
		   (o2o-command-continue-previous-quote) (org-in-block-p ("my")) 'delete)			
		  ("\\(?:^@[[:space:]]*L$\\|^LINK[[:space:]]*$\\)"
		   (o2o-command-combine-with-previous-heading) (org-in-block-p ("my")) 'delete)
		  ;; Retitling headers
		  ("\\(?:\\(?:Highlight\\|Underline\\) on page\\)"
		   (o2o-command-retitle-header-from-next-quote) (org-at-heading-p))
		  ("Squiggly on page"
		   (o2o-command-retitle-header-from-next-quote-and-crit-tag) (org-at-heading-p))
		  ("\\(?:^@[[:space:]]*\\|^TITLE \\)" ;; last after all other @ starting commands
		   (o2o-command-set-title) (org-in-block-p ("my")) 'delete)
		  ;; Adding to the titles
		  ("^TODO[[:space:]]* "
		   (o2o-command-set-todo) (org-in-block-p ("my")) 'delete)
		  ("^REL "
		   (o2o-command-set-relate-todo) (org-in-block-p ("my")) 'delete)
		  ("^TAG "
		   (o2o-command-set-tag) (org-in-block-p ("my")))
		  ("^REF[[:space:]]*"
		   (o2o-command-set-ref-todo) (org-in-block-p ("my")) 'delete))))



(use-package vic-bib-functions
  :straight (:local-repo "~/.emacs.d/inits" :files ("vic-bib-functions.el")))


;;;; PDFs

(defcustom vic/pdf-annotated-filename-regexp "_annotated"
  "Regexp identifying filenames of annotated pdfs")

;;; Bibtex
;;; Bibtex-completion

;; Do I need this? 2024-10-07
;; (use-package bibtex-completion
;;   :custom
;;   (bibtex-completion-bibliography '("~/bib/Vbib.bib"))
;;   (bibtex-completion-library-path '("~/bib/pdfs/"))
;;   (bibtex-completion-notes-path "~/notes/bib/")
;;   (bibtex-completion-notes-extension ".org")
;;   (bibtex-completion-find-additional-pdfs t)
;;   (bibtex-completion-pdf-extension '(".pdf" ".html" ".epub" )))

(use-package bibtex
  :custom
  (bibtex-dialect 'biblatex)
  (bibtex-files '(bibtex-file-path))
  (bibtex-autokey-year-length 0)
  (bibtex-autokey-names 2)
  (bibtex-autokey-names-stretch 1)
  (bibtex-autokey-additional-names "EtAl")
  (bibtex-autokey-name-case-convert-function 'capitalize)
  (bibtex-autokey-name-year-separator nil)
  (bibtex-autokey-year-title-separator "-")
  (bibtex-autokey-titleword-separator "")
  (bibtex-autokey-titlewords 2)
  (bibtex-autokey-titlewords-stretch 1)
  (bibtex-autokey-titleword-length "infnty")
  (bibtex-autokey-titleword-case-convert-function 'capitalize)
  (bibtex-autokey-before-presentation-function 'vic/asciify-string)
  (bibtex-autokey-titleword-ignore '("Le" "le" "Les" "les" "La" "la" "de" "Des" "des" "Une" "une" "Un" "un" "A" "An" "On" "The" "Eine?" "Der" "Die" "Das" "[^[:upper:]].*" ".*[^[:upper:][:lower:]0-9].*"))
  :config
  ;; set bibtex-entry-alist. Since the list is very long, manage it in other file
  (load-file (concat vic/init-dir "set-bibtex-entry-alist.el")))

;;; Biblio
(use-package biblio
  :custom (biblio-bibtex-use-autokey t))
;;; Citar
(use-package citar
  :after oc
  ;; TODO Need this ?
  ;; (advice-add #'completing-read-multiple :override #'consult-completing-read-multiple)
  :custom
  (citar-bibliography '("~/bib/Vbib.bib"))
  (citar-notes-paths '("~/notes/bib/"))
  (citar-library-paths '("~/bib/pdfs/"))
  (citar-citeproc-csl-styles-dir "~/bib/styles/csl")
  (citar-citeproc-csl-style "institut-national-de-la-recherche-scientifique-sciences-sociales.csl")
  (citar-citeproc-csl-locales-dir "~/bib/styles/locales")
  (citar-format-reference-function 'citar-citeproc-format-reference)
  (org-cite-global-bibliography '("~/bib/Vbib.bib"))
  (org-cite-insert-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-activate-processor 'citar)

  :config
  (require 'citar-citeproc)
  (defun vic/citar-xclip-html-references (&optional arg)
  "Select references and copy them to clipboard in html format.
Invoked with universal argument C-u, also select a csl style
for the output."
      (interactive "P")
      (let* ((keys (citar-select-refs))
	     (tmp-file (file-name-concat temporary-file-directory "citar_to_xclip"))
	     (style (if (or arg (not citar-citeproc-csl-style))
			(expand-file-name (citar-citeproc-select-csl-style) citar-citeproc-csl-styles-dir)
		      (if (string-match-p "/" citar-citeproc-csl-style)
			  citar-citeproc-csl-style
			(expand-file-name citar-citeproc-csl-style citar-citeproc-csl-styles-dir))))
	     (bibs (citar--bibliography-files))
	     (proc (citeproc-create style
				    (citeproc-hash-itemgetter-from-any bibs)
				    (citeproc-locale-getter-from-dir citar-citeproc-csl-locales-dir)
				    "en-US"))
	     (html-string (car (progn
			   (citeproc-add-uncited keys proc)
			   (citeproc-render-bib proc 'html)))))
	(call-process-region html-string nil "xclip" nil nil nil "-i" "-t" "text/html" "-sel" "clip")))


  :bind
  ("C-c b" . 'citar-open)
  ("C-c B" . 'citar-insert-citation)
  ("M-à" . 'citar-insert-preset)
  
  :hook
  ;; Completion at point for bibliography references
  (LaTeX-mode . citar-capf-setup)
  (org-mode . citar-capf-setup))

(use-package citar-embark
  :after citar embark
  :no-require
  :config (citar-embark-mode)
  :custom
  (citar-at-point-function 'embark-act)
  :bind
  (:map citar-embark-map
	("g" . 'vic/citar-pull-files-from-sync)
	("x" . 'vic/citar-xclip-html-references)
	("s" . 'vic/citar-push-files-to-sync)))


;; From Xah Lee : remove ascii chars from bibtex keys
(defun vic/asciify-text (&optional @begin @end)
  "Remove accents in some letters and some
Change European language characters into equivalent ASCII ones, e.g. “café” ⇒ “cafe”.
When called interactively, work on current line or text selection.

URL `http://ergoemacs.org/emacs/emacs_zap_gremlins.html'
Version 2018-11-12"
  (interactive)
  (let (($charMap
         [
          ["ß" "ss"]
          ["á\\|à\\|â\\|ä\\|ā\\|ǎ\\|ã\\|å\\|ą\\|ă\\|ạ\\|ả\\|ả\\|ấ\\|ầ\\|ẩ\\|ẫ\\|ậ\\|ắ\\|ằ\\|ẳ\\|ặ" "a"]
          ["æ" "ae"]
          ["ç\\|č\\|ć" "c"]
          ["é\\|è\\|ê\\|ë\\|ē\\|ě\\|ę\\|ẹ\\|ẻ\\|ẽ\\|ế\\|ề\\|ể\\|ễ\\|ệ" "e"]
          ["í\\|ì\\|î\\|ï\\|ī\\|ǐ\\|ỉ\\|ị" "i"]
          ["ñ\\|ň\\|ń" "n"]
          ["ó\\|ò\\|ô\\|ö\\|õ\\|ǒ\\|ø\\|ō\\|ồ\\|ơ\\|ọ\\|ỏ\\|ố\\|ổ\\|ỗ\\|ộ\\|ớ\\|ờ\\|ở\\|ợ" "o"]
          ["ú\\|ù\\|û\\|ü\\|ū\\|ũ\\|ư\\|ụ\\|ủ\\|ứ\\|ừ\\|ử\\|ữ\\|ự"     "u"]
          ["ý\\|ÿ\\|ỳ\\|ỷ\\|ỹ"     "y"]
          ["þ" "th"]
          ["ď\\|ð\\|đ" "d"]
          ["ĩ" "i"]
          ["ľ\\|ĺ\\|ł" "l"]
          ["ř\\|ŕ" "r"]
          ["š\\|ş\\|ś" "s"]
          ["ť\\|ţ" "t"]
          ["ž\\|ź\\|ż" "z"]
          [" " " "]       ; thin space etc
          ["–" "-"]       ; dash
          ["—\\|一" "--"] ; em dash etc
          ])
        $begin $end
        )
    (if (null @begin)
        (if (use-region-p)
            (setq $begin (region-beginning) $end (region-end))
          (setq $begin (line-beginning-position) $end (line-end-position)))
      (setq $begin @begin $end @end))
    (let ((case-fold-search t))
      (save-restriction
        (narrow-to-region $begin $end)
        (mapc
         (lambda ($pair)
           (goto-char (point-min))
           (while (search-forward-regexp (elt $pair 0) (point-max) t)
             (replace-match (elt $pair 1))))
         $charMap)))))

(defun vic/asciify-string (@string)
  "Returns a new string. European language chars are changed ot ASCII ones e.g. “café” ⇒ “cafe”.
See `vic/asciify-text'
Version 2015-06-08"
  (with-temp-buffer
      (insert @string)
      (vic/asciify-text (point-min) (point-max))
      (buffer-string)))

(defun vic/unspace-string (string)
  (replace-regexp-in-string " " "_" string))





;;; Nov
(use-package nov
  :config
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode)))
