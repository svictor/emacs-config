;; Org-mode settings

;;; Basic settings
(use-package org
  :mode (("\\.org$" . org-mode))
  :ensure t
  :custom
  (org-yank-adjusted-subtrees t)
  (org-yank-folded-subtrees t)
  (org-catch-invisible-edits 'show-and-error)
  (org-use-property-inheritance '("DIR"))  ; Property inheritance also affects attachments
  (org-footnote-define-inline t) ; inline footnotes because easier to copy/paste/refile
  (org-footnote-auto-adjust t) ; renumber and sort at each insertion
  (org-footnote-section nil); place footnotes right at end of section
  (org-special-ctrl-a/e t) ; goto beg/end of logical headline first
  (org-use-speed-commands t)
  (org-archive-location "zzz_archives/%s_archive::")
  ;; Beauty
  (org-fontify-quote-and-verse-blocks t)
  (org-auto-align-tags nil)
  (org-tags-column 0)
  (org-hide-emphasis-markers nil)
  (org-pretty-entities tab-width)
  (org-pretty-entities-include-sub-superscripts nil)
  (org-startup-indented t)
  (org-startup-folded 'show2levels)
  (org-log-done 'time)
  (org-image-actual-width '(300)) ; get width from "#+ATTR_*: :width
					; x " and fallback on this if none
					; found  
  ;;;;; Open links right
  (org-file-apps '(("mov" . "vlc %s")
		   ("ogg" . "vlc %s")
		   ("mov" . "vlc %s")
		   ("mkv" . "vlc %s")
		   ("\\.od[t|p|f]\\'" . "soffice %s")
		   ("\\.docx?\\'" . "soffice %s")		     
		   ("\\.mm\\'" . default)
		   ("\\.x?html?\\'" . default)
		   ("\\.pdf\\'" . default)
		   ("\\.sv\\'" . "sonic-visualiser %s")
		   (auto-mode . emacs)))

  
  ;; Capture
  (org-capture-templates
   '(("i" "Inbox" entry (file "~/org/in.org")
      "* %i%?\n%i" :kill-buffer t)
     ("t" "Todo" entry (file "~/org/todo.org")
      "* TODO %i%?\n%i" :kill-buffer t)
     ("T" "Next" entry (file "~/org/todo.org")
      "* NEXT %i%?\n%i" :kill-buffer t)
     ("F" "Forthcoming" entry (file+headline "~/org/tickler.org" "Occasionels")
      "* TODO %^{Titre}\nSCHEDULED: %^t\n%i%?" :kill-buffer t)
     
     ;; Thoughts and references
     ("a" "Audio/video with position (from EMMS)" entry (file "~/notes/av_notes.org")
      ;; if org-mm-relative-file-names is non-nil, need to let-bind default-directory for org-capture template
      "* %^{Titre} :detail:\n :PROPERTIES:\n :CAPTURED: %U\n :MEDIA_FILE: %(let ((default-directory \"~/notes/\")) (org-mm-fname-make (emms-track-name (emms-playlist-current-selected-track))))\n :MEDIA_START: %(number-to-string emms-playing-time)\n :END:\n %?")
     ;; if org-mm-relative-file-names is non-nil, need to let-bind default-directory for org-capture template
     ("A" "Audio/Video whole track" entry (file "~/notes/av_notes.org")
      "* %^{Titre}\n :PROPERTIES:\n :CAPTURED: %U\n :MEDIA_FILE: %(let ((default-directory \"~/notes/\")) (org-mm-fname-make (emms-track-name (emms-playlist-current-selected-track))))\n :MEDIA_START: 0\n :END:\n %?")
     ("r" "Références")
     ("ro" "Ordis" entry (file "~/notes/references_ordis.org")
      "* %^{Titre} \n:PROPERTIES:\n:CAPTURED: %U\n:END:\n%i%?" :kill-buffer t)
     ("ra" "Anthro" entry (file "~/notes/references_anthro.org")
      "* %^{Titre} \n:PROPERTIES:\n:CAPTURED: %U\n:END:\n%i%?" :kill-buffer t)
     ("rg" "Référence générale" entry (file "~/notes/references_general.org")
      "* %^{Titre}\n:PROPERTIES:\n:CAPTURED: %U\n:END:\n%i%?" :kill-buffer t)
     
     ;; This will only be shown in mail buffers. Goes directly to current projects
     ("m" "Mail (répondre)" entry (file "~/org/repondre.org")
      "* RÉPONDRE %a :mail:\n" :prepend t :kill-bufer t)))

  ;; Todo
  (org-todo-keywords
   '((type "FORTH(f)" "TODO(t)" "NEXT(n)" "LATER(l)" "RÉPONDRE(r)" "|" "DONE(d)" "CANCEL(c)")
     (type "READ" "SEE" "LISTEN" "|" "DONE(d)" "CANCEL(c)")))
  
  (org-todo-keyword-faces
	'(("FORTH" . (:foreground "dim gray"))
	  ("TODO" . (:foreground "coral1" :weight bold))
	  ("NEXT" . (:foreground "dark orange" :weight bold))
	  ("HOLD" . "brown")
	  ("LATER" . (:foreground "dark green"))
	  ("READ" . org-footnote)
	  ("SEE" . org-footnote)
	  ("LISTEN" . org-footnote)
	  ("RÉPONDRE" . compilation-warning)))

  (org-tag-faces
	'(("lire" . org-sexp-date)
	  ("notes" . org-sexp-date)
	  ("mail" . compilation-warning)
	  ("tel" . compilation-warning)
	  ("ordis" . "Magenta")
	  ("webm" . "Magenta")))

  (org-log-into-drawer t)
  (org-enforce-todo-dependencies t)
  
  ;; Only show these templates in mail read buffers
  (org-capture-templates-contexts
   '(("m" ((in-mode . "mu4e-view")))        
     ("a" ((lambda () (and (emms-playlist-current-selected-track) (bound-and-true-p emms-playing-time)))))
     ("A" ((lambda () (emms-playlist-current-selected-track))))))
  
  :config
  ;;;;; Custom blocks
  ;; TODO Why can’t I set this in :custom block ? 
  (setq org-structure-template-alist
	(delete-dups (append
		      '(("m" . "my")
			("p" . "prez")
			("t" . "todo")
			("L" . "src lilypond")
			("d" . "details"))
		      org-structure-template-alist)))
  
  ;; Position point nicely in custom blocks
  (advice-add 'org-insert-structure-template :after
	      (lambda (type)
		(if (use-region-p)
		    (goto-char (region-end))
		  (open-line 1)))
	      '((name . "open line")))

  ;; Encrypt entries with :crypt: tag
  (org-crypt-use-before-save-magic)
  (setq  org-tags-exclude-from-inheritance (quote ("crypt"))
	 org-crypt-key "Victor A. Stoichita <svictor@svictor.net>")
  
  ;; Open links in same window
  (setf (cdr (assoc 'file org-link-frame-setup)) 'find-file)
  
  :bind
  ("C-c L" . 'org-insert-link-global)
  ("C-c o" . 'org-open-at-point-global)
  ("C-c l" . 'org-store-link)
  ("C-c a" . 'org-agenda)
  ("C-c c" . 'org-capture)
  (:map org-mode-map
	("C-\"" . 'org-mark-ring-goto)
	("C-c «" . 'org-mark-subtree)
	("C-c C-x t" . 'org-inlinetask-insert-task)))

;;; Babel
;; Babel is part of org package, but I set it up separately
(org-babel-do-load-languages
  'org-babel-load-languages
  '((emacs-lisp . t)
    (shell t)
    (org t)
    (lilypond t)))

(defun vic/org-babel-noconfirm-p (language body)
  "Determine whether orgmode should skip asking for confirmation before executing babel blocks for LANGUAGE."
  (when (string= language "lilypond")
    (message "bypass")
    t)
  nil)


(defun vic/org-src-debug ()
  "Put a call to this function at the beginning of the org source block to debug it.
If there is a sequence of calls, temporarily wrap them by progn
From https://emacs.stackexchange.com/questions/13244/edebug-orgmode-source-code-blocks-with-input-variables"
  (save-excursion
    (let ((pt (let ((case-fold-search t)) (org-babel-where-is-src-block-head))))
      (unless pt (error "Not at source block"))
      (goto-char pt)
      (org-edit-src-code)
      (let ((parse-sexp-ignore-comments t))
        (goto-char (point-min))
        (forward-sexp 2)
        (edebug-defun)))))


(setq org-confirm-babel-evaluate 'vic/org-babel-noconfirm-p)

;; For lilypond mostly
(add-hook 'org-babel-after-execute-hook 'org-redisplay-inline-images)
;; Indent with tab as in major mode
(setq org-src-tab-acts-natively t)
(setq org-babel-lilypond-midi-command "timidity -ia")
;; Expand noweb references by default
(setq org-babel-default-header-args
      (cons '(:noweb . "yes")
            (assq-delete-all :noweb org-babel-default-header-args)))
;; Cache results by default
(setq org-babel-default-header-args
      (cons '(:cache . "yes")
            (assq-delete-all :cache org-babel-default-header-args)))

;;; Export
(setq 
 org-export-with-toc nil
 org-export-with-author nil
 org-html-postamble nil
 org-export-backends (quote (ascii html latex beamer odt))
 org-export-allow-bind-keywords t ; allow to bind variables locally with #+BIND keyword
 org-export-with-broken-links 'mark)

(setq 
 org-icalendar-include-todo (quote t)
 org-icalendar-timezone "GMT+2 CEST"
 org-icalendar-use-scheduled (quote (event-if-not-todo todo-start)))

(setq org-latex-compiler "lualatex"
      org-latex-pdf-process '("latexmk -f -pdf -%latex -interaction=nonstopmode -output-directory=%o %f" "latexmk -c")
      org-latex-with-hyperref nil
      org-latex-hyperref-template "\\hypersetup{\n colorlinks=true,linkcolor=blue}\n")

(setq org-latex-classes '(("beamer" "\\documentclass[presentation,hyperref={colorlinks}]{beamer}"
			  ("\\section{%s}" . "\\section*{%s}")
			  ("\\subsection{%s}" . "\\subsection*{%s}")
			  ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
			 ("article" "\\documentclass[11pt]{article}"
			  ("\\section{%s}" . "\\section*{%s}")
			  ("\\subsection{%s}" . "\\subsection*{%s}")
			  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
			  ("\\paragraph{%s}" . "\\paragraph*{%s}")
			  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
			 ("report" "\\documentclass[11pt]{report}"
			  ("\\part{%s}" . "\\part*{%s}")
			  ("\\chapter{%s}" . "\\chapter*{%s}")
			  ("\\section{%s}" . "\\section*{%s}")
			  ("\\subsection{%s}" . "\\subsection*{%s}")
			  ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
			 ("book" "\\documentclass[11pt]{book}"
			  ("\\part{%s}" . "\\part*{%s}")
			  ("\\chapter{%s}" . "\\chapter*{%s}")
			  ("\\section{%s}" . "\\section*{%s}")
			  ("\\subsection{%s}" . "\\subsection*{%s}")
			  ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(setq org-latex-packages-alist nil)
(add-to-list 'org-latex-packages-alist
             '("AUTO" "babel" t ("pdflatex")))
(add-to-list 'org-latex-packages-alist
             '("AUTO" "polyglossia" t ("xelatex" "lualatex")))
(add-to-list 'org-latex-packages-alist
             '("" "fontspec" nil ("xelatex" "lualatex")))
(add-to-list 'org-latex-packages-alist
             '("" "multimedia" t ("lualatex")))

(defun vic/org-put-export-properties ()
  "Create some export properties for the current header"
  (interactive)
  (let* ((hl (org-entry-get nil "ITEM"))
	 (htitle (read-from-minibuffer "EXPORT_TITLE: " nil nil nil 'export-props hl))
	 (title (org-get-title)))

    (if title
	(progn
	  (org-set-property "EXPORT_TITLE" title) ; if document title available use that
	  (org-set-property "EXPORT_SUBTITLE" htitle))
      (org-set-property "EXPORT_TITLE" htitle))

    ;; add 2 or 3 variants of htitle to the history of export props.
    (setq title-ascii-nospace (vic/unspace-string (vic/asciify-string htitle)))
    (when title
      (add-to-list 'export-props (concat (vic/unspace-string title) "-" title-ascii-nospace)))
    (add-to-list 'export-props (concat (file-name-base (buffer-file-name)) "-" title-ascii-nospace))
    (add-to-list 'export-props title-ascii-nospace)
    
    (org-set-property "EXPORT_FILE_NAME"
		      (read-from-minibuffer "EXPORT_FILE_NAME: " nil nil nil 'export-props))

    (org-set-property "EXPORT_OPTIONS"
	 (concat (if (yes-or-no-p "Number headings? ") "num:t " "num:nil ")))))

;;; Agenda
(setq org-agenda-tags-column 0
      org-agenda-block-separator ?-
      org-agenda-time-grid '((daily today require-timed)
			     (800 1000 1200 1400 1600 1800 2000)
			     " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
      ;; org-agenda-use-time-grid nil ; toggle it with G when needed
      org-agenda-current-time-string	"⭠ now ─────────────────────────────────────────────────"    
      ;; NB: Hit C-<number> C-c a to display agenda for next <number> days
      org-agenda-restore-windows-after-quit t
      ;; org-deadline-warning-days 3	; warn me of deadlines in the next 5 days
      ;; org-agenda-skip-deadline-prewarning-if-scheduled t ; but don’t annoyingly warn before deadline if already scheduled
      org-agenda-skip-scheduled-if-done t
      org-agenda-skip-deadline-if-done t
      org-agenda-todo-ignore-scheduled 'future ; dont't display things scheduled for the future
      org-agenda-tags-todo-honor-ignore-options t ; ignore options for todo also applies to tags-todo
      org-agenda-start-on-weekday 1  ; weeks start on Monday
      org-stuck-projects '("+encours+LEVEL=1/-DONE" ("*") nil "")
	
      org-agenda-prefix-format '((agenda . "%?-12t% s")
				 (todo . "%i %-12:c%?-12t%b")
				 (tags . "%i %-12:c%?-12t%b")
				 (search . "%i %-12:c"))	
      org-agenda-files '("~/org/"))

  ;; Agenda views 
(setq org-agenda-custom-commands
'(("v" "Agenda sans todo" agenda* ""
   ;; ensures that repeating events appear on all relevant dates
   ((org-agenda-repeating-timestamp-show-all t)))
  ;; ("xh" "Maison"
  ;;  ((todo "NEXT|FORTH"
  ;; 	  ((org-agenda-prefix-format "  %i %-15:c [%e] ")
  ;; 	   (org-agenda-files '("~/maison/"))))
  ;;   (todo ""
  ;; 	  ((org-agenda-prefix-format "  %i %-15:c ")
  ;; 	   (org-agenda-skip-function '(org-agenda-skip-entry-if 'todo '("NEXT")))
  ;; 	   (org-agenda-files '("~/maison/"))))))
  ;; ("xa" "Academia"
  ;;  ((todo "NEXT|FORTH"
  ;; 	  ((org-agenda-prefix-format "  %i %-15:c [%e] ")
  ;; 	   (org-agenda-files '("~/academia/"))))
  ;;   (todo ""
  ;; 	  ((org-agenda-prefix-format "  %i %-15:c ")
  ;; 	   (org-agenda-skip-function-global '(org-agenda-skip-entry-if 'todo '("NEXT")))
  ;; 	   (org-agenda-files '("~/academia/"))))))
  ;; ("xp" "Projects"
  ;;  ((tags-todo "project"
  ;;              ((org-agenda-prefix-format "  %i %-15:c [%e] ")
  ;; 		(org-agenda-skip-function '(org-agenda-skip-entry-if 'nottodo '("NEXT")))
  ;; 		(org-agenda-overriding-header "Projects NEXT")))
  ;;   (tags-todo "project"
  ;;              ((org-agenda-prefix-format "  %i %-15:c ")
  ;; 		(org-agenda-skip-function '(org-agenda-skip-entry-if 'todo '("NEXT")))
  ;; 		(org-agenda-overriding-header "Projects TODO")))))
  ("n" "Now"
   ((agenda "" ; daily agenda
            ((org-agenda-span-name 'day)
	     (org-deadline-warning-days 3)
	     (org-agenda-skip-function '(org-agenda-skip-entry-if 'scheduled))))
    (todo "FORTH" ; tickler
          ((org-agenda-skip-function '(org-agenda-skip-entry-if 'notscheduled 'todo '("NEXT")))
	   (org-agenda-todo-ignore-scheduled 'future)
           (org-agenda-overriding-header "\nPense-bête")))
    (todo "NEXT"
          ((org-agenda-prefix-format "  %i %-12:c [%e] ")
           (org-agenda-overriding-header "\nNext\n")))
    (tags-todo "-noagenda+mail|-noagenda+tel|-noagenda+dire"
	       ((org-agenda-prefix-format '((tags . "%i %?-12t%b")))
		(org-agenda-overriding-header "Dire")))))
  ("d" "Agenda & todo global"
   ((agenda "" ; weekly agenda
            ((org-agenda-span 7)
	     (org-agenda-start-on-weekday nil) ; start agenda today
	     (org-agenda-skip-function '(org-agenda-skip-entry-if 'scheduled))
	     (org-deadline-warning-days 0)))
    (todo "" ; tickler
          ((org-agenda-skip-function '(org-agenda-skip-entry-if 'notscheduled 'todo '("NEXT")))
	   (org-agenda-todo-ignore-scheduled 'future)
           (org-agenda-overriding-header "\nPense-bête (penses-tu?)")))
    (tags-todo "-noagenda+mail|-noagenda+tel|-noagenda+dire"
	       ((org-agenda-prefix-format '((tags . "%i %?-12t%b")))
		(org-agenda-overriding-header "Dire")))
    (todo "NEXT"
          ((org-agenda-prefix-format "  %i %-12:c [%e] ")
           (org-agenda-overriding-header "\nNext\n")))
    (todo "TODO"
          ((org-agenda-prefix-format "  %i %-12:c [%e] ")
           (org-agenda-overriding-header "\nTodo\n")))
    
    (tags-todo "inbox"
               ((org-agenda-prefix-format "  %?-12t% s [%e]")
		(org-agenda-files '("~/org/in.org" "~/org/aphone_in.org"))
		(org-agenda-overriding-header "\nInbox\n")))
    (tags "CLOSED>=\"<today>\""
          ((org-agenda-overriding-header "\nCompleted today\n")))))))

;;; Attach

(use-package org-attach
  :straight nil

  :custom
  (org-attach-method 'mv)
  (org-attach-preferred-new-method 'dir)
  (org-attach-dir-relative t)

  :hook
  ;; Attach marked files in dired to current org entry
  (dired-mode-hook . (lambda ()
		       (define-key dired-mode-map
				   (kbd "C-c C-x a")
				   #'org-attach-dired-to-subtree))))
;;; Clock
(use-package org-clock
  :straight nil

  :custom
  (org-clock-idle-time 10)
  (org-clock-persist 'history)
  (org-clock-persist-file "~/.emacs.d/.autogeneres/org-clock-save.el")
  (org-clock-out-remove-zero-time-clocks t)
  (org-clock-auto-clock-resolution (quote when-no-clock-is-running)) ; auto clock resolution
  (org-clock-report-include-clocking-task t) ; include current clocked task
  (org-clock-out-when-done t)
  ;; global clock values
  (org-global-properties '(("EFFORT_ALL" . "10m 30m 1h 2h 3h 4h 5h 6h 7h 8h 1d 2d 3d")))
  ;; Also set a default columns format that shows effort estimates
  (org-columns-default-format "%40ITEM(Task) %17Effort(Estimated Effort){:} %CLOCKSUM")

  :config
  (org-clock-persistence-insinuate)

  ;; Clock into the current task from the agenda
  (defun vic/org-clock-in ()
    (interactive)
    (org-clock-in '(4)))

  :bind
  ("C-c I" . #'vic/org-clock-in)
  ("C-c O" . #'org-clock-out))


;;; Refile
(setq  org-refile-targets `((nil :maxlevel . 5) ;current buffer up to level 5
			    (org-agenda-files :maxlevel . 5)
			    ;; (,(directory-files-recursively "~/bib/notes/" "\\.org$") :maxlevel . 3)
			    ("~/notes/idees.org"  :level . 1))
       org-refile-allow-creating-parent-nodes 'confirm
       org-outline-path-complete-in-steps nil
       org-refile-use-outline-path 'file)


;;; Copy-paste from html
(use-package org-copy-paste-html
  :straight (:local-repo "~/.emacs.d/inits" :files ("org-copy-paste-html.el"))
  :bind
  (:map org-mode-map ("C-Y" . #'org-cph-yank-html-clipboard))
  (:map org-mode-map ("M-W" . #'org-cph-copy-region-to-rich-clipboard))
  :config
  (with-eval-after-load "message-mode" (bind-key "C-Y" #'org-cph-yank-html-clipboard message-mode-map)))

;;; Org-web-tools
(use-package org-web-tools
   :bind (:map org-mode-map
 	      ("C-x c w" . 'org-web-tools-insert-link-for-url)
 	      ("C-x c W" . 'org-web-tools-insert-web-page-as-entry)))

;;; Org-reveal
(use-package ox-reveal
  :defer 5
  :custom
  ;; TODO Ensure that reveal.js and custom css/js are downloaded as well, for reproducibility
  (org-reveal-root "file:///home/vic/ordis/src/reveal.js/")
  (org-reveal-extra-css (concat org-reveal-root "victor/victor.css"))
  (org-reveal-extra-script-src (concat org-reveal-root "victor/victor.js"))
  (org-reveal-theme "serif")
  (org-reveal-head-preamable "<base target=\"_blank\">")
  (org-reveal-title-slide "<h1 class=\"title\">%t</h1>
                                  <h2 class=\"author\">%a</h2>
                                  <h6>Centre de recherche en ethnomusicologie<br/>Laboratoire d'ethnologie et de sociologie comparative<br/> CNRS - Université Paris-Nanterre</h6>")
  (org-reveal-init-options "width:1280, height:800, transition:'fade', transitionSpeed:'fast'")

  :config
  (defun vic/org-remove-paragraphs-for-export (backend)
    ;; Deactivate !!!
    ;; (when nil
    (when (eq backend 'reveal)
      (goto-char (point-min))
      (org-show-all)
      (while (and (eq (forward-line) 0)
		  (< (point) (point-max)))
	(let ((line (org-current-line-string)))
	  (unless (or (string-match-p org-heading-regexp line)
		      (string-match-p org-property-re line)
		      (string-match-p org-keyword-regexp line)
		      (string= line "")
		      (org-in-block-p '("quote" "prez" "notes" "NOTES"))
		      )
	    (kill-line))))))

  :hook
  (org-export-before-processing . vic/org-remove-paragraphs-for-export))

;;; Khalel
(use-package khalel
  :commands (khalel-export-org-subtree-to-calendar
	     khalel-import-events
	     khalel-edit-calender-event
	     khalel-add-capture-template)
  :custom
  (khalel-import-org-file "~/org/khalel_import.org")
  (khalel-default-calendar nil)
  (khalel-import-start-date "-30d")
  (khalel-import-end-date "+90d")
  (khalel-default-alarm "")
  (khalel-import-org-file-confirm-overwrite nil)
  (khalel-capture-key "e")
  :config 
  (khalel-add-capture-template))

;;; IDs
(setq org-id-link-to-org-use-id 'create-if-interactive-and-no-custom-id
      org-id-method 'org ; shorter than uuid
      org-id-link-to-org-use-id 'create-if-interactive-and-no-custom-id)

(defun vic/org-custom-id-get-create (arg &optional interactive)
  "Add a CUSTOM_ID if no ID exists and called interactively. Meant to advise `org-store-link'.
The CUSTOM…ID is made from the 5 first words of the headline "
  (when (and
	 interactive
	 (buffer-file-name (buffer-base-buffer)) (derived-mode-p 'org-mode)
	 (not (org-in-regexp "[^<]<<\\([^<>]+\\)>>[^>]" 1)) ; this should be handled by org-store-link
	 (not (org-entry-get nil "CUSTOM_ID"))
	 (not (org-entry-get nil "ID"))
	 (not (org-before-first-heading-p)))	 
    (let* ((title (nth 4 (org-heading-components)))
	   (split-title (split-string title " "))
	   (short-title (mapconcat #'identity
				   (delq nil ; avoid spurious dash at end if list shorter than limit
					 (cl-loop for i below 5 collect (nth i split-title)))
				   "-"))
	   (org-id-method 'org))
      (org-entry-put nil "CUSTOM_ID" (org-id-new short-title)))))

(advice-add 'org-store-link :before #'vic/org-custom-id-get-create)

;;; Custom functions
(use-package vic-org-quirks
  :straight (:local-repo "~/.emacs.d/inits" :files ("vic-org-quirks"))
  :hook
  ;; move attachment directories upon archiving
  (org-archive . vic-org-quirks-attach-move-dir-to-archive))

