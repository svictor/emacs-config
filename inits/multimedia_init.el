;;; EMMS
(use-package emms
  :custom
  (emms-source-file-default-directory "~/Audio/")
  (emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find)
  (emms-player-list '(emms-player-mpg321)
		    ;; emms-player-ogg123
		    emms-player-mplayer-playlist
		    emms-player-mplayer
		    emms-player-mpv
		    emms-player-vlc
		    emms-player-vlc-playlist)
  (emms-history-file "~/.emacs.d/emms/history")
  (emms-player-base-format-list (append emms-player-base-format-list '("opus")))


  :config
  (emms-all)
  (emms-default-players)
  
  ;; Customize descriptions
  (defun vic-emms-track-description (track)
    "Return a somewhat nice track description."
    (let ((artist (emms-track-get track 'info-artist))
          (album (emms-track-get track 'info-album))
          (tracknumber (emms-track-get track 'info-tracknumber))
          (title (emms-track-get track 'info-title)))
      (cond
       ((or artist title)
	(concat (if (> (length artist) 0) artist "Unknown artist") " - "
		(if (> (length album) 0) album "Unknown album") " - "
		(if (> (length tracknumber) 0)
                    (format "%02d" (string-to-number tracknumber))
                  "XX") " - "
                  (if (> (length title) 0) title "Unknown title")))
       (t
	(emms-track-simple-description track)))))

  (setq emms-track-description-function 'vic-emms-track-description)

  (defvar vic-av-paths '("~/audio/rowAudio/"
			 "~/audio/rromaniaAudio/"
			 "~/audio/greeceAudio/"						 
			 "~/video/rowVideo/"
			 "~/video/rromaniaVideo/")
    "Paths used by ’vic-shorten-av-paths’.")
  
  ;; Shorten displayed file names
  (defun vic-shorten-av-paths (path)
    "Shorten path to my audio-video files. Used to advise emms-tracks-simple-description."
    (let* ((av-folders-regexp (mapconcat 'expand-file-name vic-av-paths "\\|")))
					; Add space here for org-mm-nice-path-func
      (concat (replace-regexp-in-string av-folders-regexp "" path) " ")))


  (defun vic-filename-base (path)
    "Apply ‘file-name-base’ to PATH. 

  Used to advise functions in a removable way (name instead of a lambda)."
    (file-name-base path))
  
  ;; (advice-add 'emms-track-simple-description :filter-return 'vic-shorten-av-paths)
  (advice-add 'emms-track-simple-description :filter-return 'vic-filename-base)
  
:bind
  ("M-<kp-insert>" . 'emms-pause)
  ("M-<kp-right>" . 'emms-next)
  ("M-<kp-left>" . 'emms-previous)
  ("M-<kp-next>" . 'emms-seek-forward)
  ("M-<kp-end>" . 'emms-seek-backward)
  ;; ("s-S" . 'emms-shuffle)
  ("s-r" . 'emms-random)
  ("s-R" . 'emms-toggle-random-playlist)
  ("M-<kp-begin>" . 'emms)
  ("M-<kp-multiply>" . 'emms-show)
  ("M-<kp-divide>" . 'emms-play-directory-tree)
  ;; ("s-D" . 'emms-add-directory-tree)
  ("M-<kp-up>" . 'emms-play-file)
  ;; ("s-L" . 'emms-play-playlist)
  ("M-<kp-down>" . 'emms-add-find)
  ;; ("s-l" . 'emms-metaplaylist-mode-go)
  (:map dired-mode-map ("M-<kp-divide>" . 'emms-play-dired)))

;;; MPV
(use-package mpv)

;;; Subtitle editor (subed)
(use-package subed
  :straight '(:type git :host github :repo "sachac/subed" :files ("subed/*.el"))
  :ensure t
  :config
  ;; Some reasonable defaults
  (add-hook 'subed-mode-hook 'subed-enable-pause-while-typing)
  ;; As the player moves, update the point to show the current subtitle
  (add-hook 'subed-mode-hook 'subed-enable-sync-point-to-player)
  ;; As your point moves in Emacs, update the player to start at the current subtitle
  (add-hook 'subed-mode-hook 'subed-enable-sync-player-to-point)
  ;; Replay subtitles as you adjust their start or stop time with M-[, M-], M-{, or M-}
  (add-hook 'subed-mode-hook 'subed-enable-replay-adjusted-subtitle))

;;; Org-media-marks
(use-package org-media-marks
  :straight (:local-repo "org-media-marks")
  :ensure t
  :config
  (defun vic/org-insert-sequential-header (&rest args)
    "Insert a header with same title as previous one same level. Look for a numerical counter and increment it if found. If not, ask for a string to differentiate the two headings.

Uses no ARGS but must accept them to be useful as ‘org-mm-heading-format-func’."
    (save-excursion
      (org-backward-heading-same-level 1)
      (let* ((hl (or (nth 4 (org-heading-components)) ""))
	     (num (and (string-match "[0-9]+" hl)
		       (match-string-no-properties 0 hl))))
      (if num	
	  (replace-regexp-in-string num (number-to-string (+ 1 (string-to-number num))) hl)
	(read-string "Name this headline: " hl)))))
  
;;;;; Quickly insert links
  (defun vic/abbrev-make-org-mm-link ()
    "make the last word an org-mm-link if possible.
Meant to be used after abbrev expansion."
    (when org-media-marks
      (let ((pos (point-marker)))
	(set-marker-insertion-type pos t)
	(re-search-backward "^\\| " nil t)
	(set-marker pos)
	(org-mm-link-insert-and-nudge)
	(deactivate-mark)
	(goto-char pos))))
  
  (defun vic/abbrev-joue ()
    "Insert the string [joue] making it a org-mm-link if possible."
    (insert "[joue]")
    (when org-media-marks
      (set-mark (- (point) 10))
      (org-mm-link-insert-and-nudge)
      (deactivate-mark)))

  (defun vic/org-mm-nice-path-func (path)
    (let* ((org-mm-cursor-track-links t)
	   (prev-file (plist-get (cdr (org-mm-prev)) :fname))
	   (org-mm-relative-file-names t))
      (if (string= prev-file path)
	  ""
	(org-mm-fname-make path))))
  
  :custom
  ;; (org-mm-heading-format-func 'vic/org-insert-sequential-header)
  ;; (org-mm-heading-format-func nil)
  (org-mm-mpv-default-options '("--geometry=10:10" "--osd-level=2" "--osd-fractions" "--force-window" "--hr-seek=yes"))
  (org-mm-nice-path-func 'vic/org-mm-nice-path-func)

  :bind (:map org-media-marks-map
	      ;; Toggle/cycle some options
	      ("C-<kp-divide>" . 'org-mm-endsec-action-cycle)
	      ("C-S-<kp-divide>"  . 'org-mm-cursor-track-links-toggle)
	      ;; Move
	      ("C-<kp-right>"  . 'org-mm-next-goto)
	      ("C-<kp-left>" . 'org-mm-prev-goto)
	      ("C-<kp-begin>" . 'org-mm-playing-goto)
	      ;; Playback
	      ("C-S-<kp-0>" . 'org-mm-insert-media-file) ;; with S kp behaves differently
	      ("C-<kp-insert>" . 'org-mm-link-or-heading-play)
	      ("<kp-insert>" . 'org-mm-tc-pause-toggle)
	      ;; Info
	      ("C-<kp-multiply>" . 'org-mm-display-position)
	      ;; Speed
	      ("C-+" . 'org-mm-tc-speed-faster)
	      ("C--" . 'org-mm-tc-speed-slower)
	      ("C-−" .  'org-mm-tc-speed-reset)

	      ;; Seek
	      ("<kp-next>" . 'org-mm-tc-seek-fwd-normal)
	      ("C-<kp-next>" . 'org-mm-tc-seek-fwd-small)
	      ("C-S-<kp-next>" . 'org-mm-tc-seek-fwd-big)
	      ("<kp-end>" . 'org-mm-tc-seek-bwd-normal)
	      ("C-<kp-end>" . 'org-mm-tc-seek-bwd-small)
	      ("C-S-<kp-end>" . 'org-mm-tc-seek-bwd-big)
	      ("<kp-down>" .  'org-mm-locate)

	      ;; Insert and update marks
	      ("<kp-enter>" . 'org-mm-heading-insert)
	      ("C-<kp-enter>" . 'org-mm-heading-insert-and-nudge)
	      ("S-<kp-enter>" . 'org-mm-link-insert-or-update)
	      ("C-S-<kp-enter>" . 'org-mm-link-insert-and-nudge)
	      ("M-<kp-enter>" . 'org-mm-link-or-heading-update)
	      ("<kp-up>" . 'org-mm-link-or-heading-update)
	      ("<kp-prior>" . 'org-mm-nudge-fwd-normal)
	      ("M-<kp-prior>" . 'org-mm-nudge-fwd-small)
	      ("M-S-<kp-prior>" . 'org-mm-nudge-fwd-big)
	      ("<kp-home>" . 'org-mm-nudge-bwd-normal)
	      ("M-<kp-home>" . 'org-mm-nudge-bwd-small)
	      ("M-S-<kp-home>" . 'org-mm-nudge-bwd-big)))

