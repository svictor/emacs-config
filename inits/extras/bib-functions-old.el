;;; Ebib functions used to convert my zotero db

(defun vic-make-properties-from-header ()
  "With point on an org-mode headline formatted as Author (date): title", extract 
  (unless (eq 'headline (car (org-element-context)))
    (user-error "Not an org-mode headline"))
  (let ((head (org-element-property :raw-value (org-element-context)))
	;; (head test)
	(author nil)
	(date nil)
	(title nil))
    (if (string-match "^.*? (" head)
	(setq author
	      (substring (match-string-no-properties 0 head) 0 -2))
      (setq author "No author"))
    (if (string-match "([0-9]...)" head)
	(setq date
	      (replace-regexp-in-string "[(|)]" "" (match-string-no-properties 0 head)))
      (setq date "No date"))
    (if (string-match ": .*$" head)
	(setq title
	      (substring (match-string-no-properties 0 head) 2))
      (setq title "No title"))
    
    (org-set-property "REF_AUTHOR" author)
    (org-set-property "REF_TITLE" title)
    (org-set-property "REF_DATE" date)))

(defun vic-make-individual-note-file ()
  "Make one note file from headline ."
  (let* ((headline (org-element-context))
	 (path "~/bib/notes/")
	 (ext ".org")
	 (prop (org-element-property :CUSTOM_ID headline)))
    (unless prop
      (user-error (format "No CUSTOM_ID property in %s" headline)))
    (org-copy-subtree)
    (find-file (concat path prop ext))
    (erase-buffer)
    (yank)
    (goto-char (point-min))
    ;; Transfer some properties to the top.
    ;; But then org-noter can't open the file straight away
    ;; (insert (format "#+title: %s\n" (org-element-property :REF_TITLE headline)))
    ;; (insert (format "#+author: %s\n" (org-element-property :REF_AUTHOR headline)))
    ;; (insert (format "#+date: %s\n\n" (org-element-property :REF_DATE headline)))
    ;; (org-delete-property "REF_TITLE")
    ;; (org-delete-property "REF_AUTHOR")
    ;; (org-delete-property "REF_DATE")
    (save-buffer)
    (kill-buffer)))
;; (defun vic/test-ebib-create-attachment-property ()
;;   (interactive)
;;   (let ((db ebib--cur-db)
;; 	(key (ebib--get-key-at-point)))
;;   (message (vic/ebib-create-attachment-property key db))))

