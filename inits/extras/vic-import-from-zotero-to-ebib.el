;;;; My functions to cleanup a bibliography exported from zotero.
;;;  through the the Better Bibtex plugin (https://retorque.re/zotero-better-bibtex/)
;;;;
;;;; Author:  Victor A. Stoichita (victor.stoichita@cnrs.fr)
;;;;
;;;; Interactive functions (see their docstrings for details)
;;;; - `vic/ebib-convert-groups-to-keywords' :: Better Bibtex uses the Groups field in the bibtex export to indicate that an entry belongs
;;;;    to one or several collections. Use this function to move the contents of the Groups field to the Keywords field adding to the
;;;;    existing keywords
;;;; - `vic/ebib-fix-zotero-keywords' :: strip from keywords those characters not suited for org tags. Add space after comma separator because that's ebib's default. 
;;;; - `vic/ebib-import-zotero-file' :: bring zotero attachments of the entry at point into the default attachment folder.
;;;;   Uses Prince from https://www.princexml.com/ to convert html attachments to pdf 
;;;; - `vic/ebib-annotation-to-org' :: convert the latex annotation field of the entry at point to a note in org-mode format
;;;;
;;;; These functions operate on the entry at point in the ebib index window.
;;;; Suggestion for batch processing: bind one of these functions to an easily accessed key.
;;;; Ex: (define-key ebib-index-mode-map (kbd "C-$") 'vic/ebib-import-zotero-file)
;;;; Then use ebib to filter the database to entries which need to be edited with that function. Those which have
;;;; - a non-empty groups field
;;;; - the base zotero path in their file field
;;;; - a non-empty annotation field
;;;; Apply the function, check the results, move on to the next entry. Optionally, record a macro to just apply-move with one key. 

;; (define-key ebib-index-mode-map (kbd "C-$") 'vic/ebib-annotation-to-note)

(require 'ebib)
(require 'vic-bib-functions) ; Needed only for vic/ebib-create-org-noter-id

;;; IMPORT ATTACHMENTS FROM ZOTERO DATABASE

(defun vic/ebib-import-zotero-file ()
  "Scan ebib entry for references to zotero attachments. 
Bringing attachment into specified directory and add it to the entry.
If Zotero filename has the annotated file identifier, keep that identifier when renaming. 
The default identifier is \"_annotated\" (default of the Zotero zotfile plugin)
If file is html convert it to pdf (with Prince)
If file is neither pdf nor html, ask what to do.
If filename exists, ask for new filename."
  (interactive)
  (let* ((zot-file-base "/home/vic/.zotero/vic_library/storage/")
	 (annotated-filename-has "_annotated")
	 (db ebib--cur-db)
	 (key (ebib--get-key-at-point))
	 ;; (key "V-test-key")
	 (files-field (ebib-get-field-value "file" key db nil t))
	 ;; (files-field zotfileshtml)   ;
	 (newpaths ()) ;; list of paths to add to entry
	 (errors nil)) ;; holds the error status of an entry 

    ;; Make list with individual file names
    (if files-field
	(setq files-list (split-string files-field ";" t))
      (return (message (format "No files for entry %s" key))))

    ;; Iterate over file names
    (dolist (elm files-list)
      ;; Get paths
      (setq zotpath (vic/ebib-import-zotero-file-find-zotpath zot-file-base elm))
      (if zotpath
	  (setq targetpath (vic/ebib-import-zotero-file-build-targetpath key zotpath annotated-filename-has))
	(error (format "Could not find pdf file for %s" elm)))      
   
      (if (and zotpath targetpath (not (string= zotpath "DELETE")))
	  (progn
	    (copy-file zotpath targetpath)
	    (move-file-to-trash zotpath)
	    (message "File copied.")
	    (push targetpath newpaths))
	;; Else
	(unless (string= zotpath "DELETE")
	  (setq errors t))))
  
    (if errors
	(error (format "Can’t copy zotero path %s to target path %s for key %s" zotpath targetpath key))
      (vic/ebib-import-zotero-file-call-ebib newpaths key db))))

(defun vic/ebib-import-zotero-file-find-zotpath (zot-file-base elm)
  "Find a zotero path in an entry exported by Better bibtex for zotero."
  (let ((zot-regex-html "text/html")
	(zot-regex-pdf "application/pdf")
	(zotpath))
    (save-match-data
      ;; Convert html to pdf
      (when (string-match (format ":\\(%s.*?\\):%s" zot-file-base zot-regex-html) elm 0)
	(setq zotpath (vic/ebib-import-zotero-file-convert-html (match-string 1 elm))))
      ;; Normal files are pdf
      (unless zotpath ;; skip if html > pdf conversion  
	(string-match (format ":\\(%s.*?\\):%s" zot-file-base zot-regex-pdf) elm 0)
	(setq zotpath (match-string 1 elm)))
      (unless zotpath ;; Last resort, for files in docx ppt etc
	(string-match (format ":\\(%s.*?\\):" zot-file-base) elm 0)
    	(setq zotpath (match-string 1 elm))
	(unless (yes-or-no-p (format "Found %s. Should I keep it?" zotpath))
	  (setq zotpath "DELETE"))))      
    (when (and zotpath (not (file-exists-p zotpath)) (not (string= zotpath "DELETE")))
      (if (yes-or-no-p (format "%s not in zotero database. Delete reference to it in bib file?" elm))
	  (setq zotpath "DELETE")
	(error "Stopping here to let you fix this." )))
    zotpath))
  
(defun vic/ebib-import-zotero-file-convert-html (zotpath)
  "Convert zotpath from html to pdf. Uses Prince from https://www.princexml.com/download/"
  (let ((wait t))
    (while wait
      (let ((choice (read-char-choice (format "What shall I do with %s: (c)onvert to pdf / (v)iew / (d)elete / (q)uit? " zotpath )'(?c ?v ?d ?q))))
        (cl-case choice
          (?q (error "quit"))
          (?c (call-process-shell-command (format "prince --media=print --page-size=A4 --no-network %s -o %s" zotpath (concat zotpath ".pdf")))
	      (setq zotpath (concat zotpath ".pdf"))
	      (unless (file-exists-p zotpath)
		(error (format "Could not create file %s" zotpath)))
	      (setq wait nil))
	  (?v (call-process-shell-command (format "see %s" zotpath)))
	  (?d (setq wait nil) (setq zotpath "DELETE"))))))
  zotpath)

(defun vic/ebib-import-zotero-file-build-targetpath (key zotpath annotated-filename-has)
  "Build a target path to copy the file from zotero storage to new directory. Handle annotated files. Don’t overwrite existing files"
  (let ((targetdir (concat (nth 0 ebib-file-search-dirs)))
	(suffix (file-name-extension zotpath)))
    ;; Body
    (setq targetpath (concat targetdir "/" key))
    ;; If file is annotated we retain the
    (when (string-match-p annotated-filename-has zotpath)
      (setq targetpath (concat targetpath annotated-filename-has)))
    (setq targetpath (concat targetpath "." suffix))
    ;; If it exists, ask for other name 
    (while (file-exists-p targetpath)
      (setq targetpath (read-file-name (format "File %s already exists. Choose another file name for %s:" targetpath zotpath) targetdir)))
    ;; Return
    targetpath))

(defun vic/ebib-import-zotero-file-call-ebib (newpaths key db)
  "Call ebib to set new file paths"
  (let ((new-field))
    (dolist (felm newpaths)
      ;; Add filename separator if new-field already populated
      (when new-field
	(setq new-field (concat new-field ebib-filename-separator)))
      (setq new-field (concat new-field (ebib--transform-file-name-for-storing felm))))
    ;; store the field
    (ebib-set-field-value "file" new-field key db 'overwrite))

  ;; Set modified flag
  (vic/ebib-set-modified-and-refresh key db))

(defun vic/ebib-clean-zotero-latex-annotation ()
  "Strip zotero specifics from an annotation"
  ;; Remove zotero links (created by zotfile) Keep the textual reference
  (replace-regexp "\\\\href{zotero\:.*?}{\\(.*?\\)}" "\\1")
  ;; Remove empty lines with ~
  (replace-regexp "^~$" "")
  ;; Replace asterisk bullets with org-mode list sign
  (replace-regexp "^* " "- "))

(defun vic/ebib-latex-annotation-to-org (&optional key db)
  "If entry at point has an annotation, return its org-mode representation. Return empty string if not."
  (let* ((key (or key (ebib--get-key-at-point)))
	 (db (or db ebib--cur-db))
	 (annot (or (ebib-get-field-value "annotation" key db t t nil) "")))
    (with-temp-buffer 
      (insert annot)
      (goto-char (point-min))
      (vic/ebib-clean-zotero-latex-annotation)
      (vic/pandoc-convert-latex-to-org 2))))

;;; ANNOTATION TO NOTE

(defun vic/ebib-annotation-to-note (&optional nodelete)
  "Using ebib convert annotation in bibtex entry to standalone note. 
   Original annotation is deleted unless NODELETE is non-nil in which case ask for confirmation"
  (interactive)
  (let ((key (ebib--get-key-at-point))
	(db ebib--cur-db)
	(nodelete (or nodelete nil))
	;; Temporarily add %A to note template
	(ebib-notes-template-specifiers (cons '(65 . vic/ebib-latex-annotation-to-org) ebib-notes-template-specifiers))
	(ebib-notes-template (concat ebib-notes-template "%A")))
    (ebib-open-note key)
    (unless nodelete
      (vic/ebib-delete-annotation-after-convert key db))))

(defun vic/ebib-set-modified-and-refresh (key db)
  "Set modified flag on datbase and refresh display"
  (ebib--set-modified t db t (seq-filter (lambda (slave)
                                           (ebib-db-has-key key slave))
                                         (ebib--list-slaves db)))
  ;; Hackishly refresh display
  (ebib-next-entry)
  (ebib-prev-entry))

(defun vic/ebib-delete-annotation-after-convert (key db)
  "Select entry buffer and delete the annotation"
  ;; Go back to index. We don’t know its buffer name therefore go to ebib entry first.
  (switch-to-buffer "*Ebib-entry*")
  (ebib-quit-entry-buffer)
  (ebib-set-field-value "annotation" nil key db 'overwrite)
  ;; Set modified flag
  (vic/ebib-set-modified-and-refresh key db))


(defun vic/ebib-convert-groups-to-keywords ()
  "Copy content of entry's Groups field to Keywords field. Delete Groups field afterwards."
  (interactive)
  (let* ((key (ebib--get-key-at-point))
	 (db ebib--cur-db)
	 (groups (ebib-get-field-value "groups" key db 'noerror 'unbraced 'xref))
	 (keywords (ebib-get-field-value "keywords" key db 'noerror 'unbraced 'xref)))
    (unless (not (or groups keywords))
      ;; concatenate
      (setq keywords (replace-regexp-in-string "^,\\|,$" "" (concat keywords "," groups)))
      ;; cleanup
      (setq keywords (vic/ebib-fix-zotero-keywords keywords))
      ;; set
      (ebib-set-field-value "keywords" keywords key db 'overwrite)
      (ebib-set-field-value "groups" nil key db 'overwrite)
      (vic/ebib-set-modified-and-refresh key db))))

(defun vic/ebib-fix-zotero-keywords (&optional keywords)
  "Add space after keyword separator (comma). Remove spaces and other characters improper for org tags"
  (interactive)
  (let* ((key (ebib--get-key-at-point))
	 (db ebib--cur-db)
	 (keywords (or keywords (ebib-get-field-value "keywords" key db 'noerror 'unbraced 'xref))))
    (when keywords
      ;; remove spaces. for consistency with org tags
      (setq keywords (replace-regexp-in-string "[  ]" "_" keywords))
      ;; remove special characters
      (setq keywords (replace-regexp-in-string "[?.\!]" "" keywords))
      ;; add space after comma
      (setq keywords (replace-regexp-in-string "," ", " keywords)))
    ;; Set the field if called interactively
    (when (called-interactively-p 'any)
      (ebib-set-field-value "keywords" keywords key db 'overwrite)
      (vic/ebib-set-modified-and-refresh key db))
    keywords))
