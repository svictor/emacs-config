;; Helper functions to extract annotations from a pdf file

(defun vic/pdf-get-all-annots (filename)
  "Get all annotations from FILENAME and export them to org. 
The exported org file is nameed like FILENAME with suffix _annots.org
If called interactively ask for a filename."
  (interactive "fExtract from pdf file: ")
    (let* ((annots (pdf-info-getannots (pdf-info-normalize-page-range nil) filename))
	   (annot-org-file-name (concat (file-name-directory filename) (file-name-base filename) "_annots.org")))
      (find-file-other-window annot-org-file-name)
       (with-current-buffer (find-buffer-visiting annot-org-file-name)
	(erase-buffer)
	(set-visited-file-name (concat (file-name-directory filename) (buffer-name)))
	(org-mode)
	(insert "#+OPTIONS: num:nil")
	(newline-and-indent)
	(dolist (annot annots)
	  (let* ((annot-content (vic/pdf-get-annot annot filename))
		 (annot-type (nth 0 annot-content))
		 (page (nth 1 annot-content))
		 (markup-text (nth 2 annot-content))
		 (contents (nth 3 annot-content)))
	    (unless (or (string= annot-type "link") (string= contents "")) ; don’t output if no content
	      (insert (concat "* p. " page))
	      (if (> (length markup-text) 100)
	      	  (insert (concat (substring markup-text 0 50) "…"))
	      	(insert markup-text))
	      (newline)
	      (insert contents)
	      (org-set-property "PAGE" page)
	      (org-set-property "MARKUP" markup-text)
	      (newline)))))))

(defun vic/pdf-get-annot (annot filename)
  "Return annotation info of one annot as a alist: page number, annotation type, markup text, contents."
  (let ((annot-type (symbol-name (cdr (assoc 'type annot))))
	(page (cdr (assoc 'page annot)))
	(markup-text "")
	(contents (cdr (assoc 'contents annot))))
    (unless (equal annot-type "link") ; skip links altogether
      (if (equal annot-type "text") ; text annots don’t have markup-text
	  (setq markup-text "[text annotation]")
	(setq markup-text (vic/pdf-get-markup-text (cdr (assoc 'markup-edges annot)) page filename))))
    (list annot-type (number-to-string page) markup-text contents)))

(defun vic/pdf-get-markup-text (markup-edges page filename)
  "Returns the document text of a markup annotation. Markup annotations can expand over several lines/regions of the document"
  (let ((markup-text ""))
    (dolist (zone markup-edges)
      (setq markup-text (concat markup-text " " (pdf-info-gettext page zone 'word filename))))
    markup-text))
