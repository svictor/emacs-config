;; Copy-paste from html
;;;###autoload
(defun org-cph-yank-html-clipboard ()
  "Yank clipboard holding rich text (rtf or html) contents into org."
  (interactive)
  (let ((targets (with-temp-buffer
		   ;; Using eshell-command because with shell-command xclip holds stdout
		   (eshell-command "xclip -o -selection clipboard -t TARGETS" 'here)
		   (buffer-string))))
    (if (string-match-p "text/html" targets)
	(let ((beg (point)))
          (kill-new (shell-command-to-string
		     "xclip -o -r -selection clipboard -t text/html | pandoc -f html -t org "))
          (yank)
	  ;; delete newlines and \0 character added by pandoc
          (delete-backward-char 4)
	  ;; cleanup pandoc additions on headlines
	  (narrow-to-region beg (point))
	  (goto-char (point-min))
	  (delete-matching-lines org-property-drawer-re)
	  (goto-char (point-min))
	  (replace-regexp org-target-regexp "")
	  (widen)
          (message "Converted yank."))
      (progn	
        (yank)
        (message "Unconverted yank.")))))


;;;###autoload
(defun org-cph-copy-region-to-rich-clipboard ()
  "Place region into X clipboard in html format and in latex format."
  (interactive)
  (let* ((region (buffer-substring-no-properties (region-beginning) (region-end)))
	 (html (shell-command-to-string (format "echo \"%s\" | pandoc -f org -t html" region))))
    
  (eshell-command (format "echo \"%s\" | nohup xclip -t \"text/html\" -sel clip > /dev/null 2>&1" html)))
  (message "Exported region to clipboard"))

(provide 'org-copy-paste-html)
