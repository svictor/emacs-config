;; From https://www.emacswiki.org/emacs/download/misc-cmds.el
(defun vic-prosequirks-to-next-word (&optional n)
  "Go to the beginning of the Nth word after point.
N defaults to 1 (next word)."
  (interactive "p")
  (if (< n 0)
      (to-previous-word (- n))
    (dotimes (_  n)
      (let ((opt  (point)))
        (skip-syntax-forward "^w")
        (when (eq opt (point))
          (skip-syntax-forward "w")
          (skip-syntax-forward "^w"))))))

(defun vic-prosequirks-to-previous-word (&optional n)
  "Go to the beginning of the Nth word before point.
N defaults to 1 (previous word)."
  (interactive "p")
  (if (< n 0)
      (to-next-word (- n))
    (dotimes (_  n)
      (let ((opt  (point)))
        (skip-syntax-backward "w")
        (when (eq opt (point))
          (skip-syntax-backward "^w")
          (skip-syntax-backward "w"))))))


;; Copied and adapted from org2org: o2o-remove-single-newlines 
(defun vic/unfill-region ()
  "Remove single newlines in active region. This is often useful for blocks of text extracted from a pdf file or from filled text buffers.

The function removes single newlines but preserves 2 or more consecutive newlines. This is useful if they should represent paragraphs."
  (interactive)
  (let ((string (buffer-substring-no-properties (point-min) (point-max)))
	(replacement))
    ;; (with-temp-buffer
    (with-temp-buffer 
      (insert string)
      (goto-char (point-min))
      (while (not (eq (point) (point-max)))
	(cond
	 ((looking-at-p "\n\n") ;; several empty lines
	  (delete-blank-lines)
	  (forward-line)) ;; reduce to 1 empty line
	 ((looking-at-p "\n")
	  ;; skip this line)
	  (forward-line))
	 ((not (eq (point) (point-min))) ;; don’t act on first line
	  (delete-backward-char 1)
	  (insert " ")))
	(forward-line))
      (setq replacement (buffer-string)))
      ;; overwrite the region
    (delete-region (point-min) (point-max))
    (insert replacement)))

(provide 'vic-prose-quirks)
