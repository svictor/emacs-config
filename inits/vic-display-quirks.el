;;;###autoload
(defun vic-dispquirks-visual-line-cycle (arg)
  "Cycle visual line modes, whitespace display or truncate long lines"
  (interactive "p")
  (pcase arg
    ;; Main cycle
    (`1 				
     (cond
      (visual-fill-column-mode
       (visual-fill-column-mode -1)
       (visual-line-mode 1)
       (previous-buffer)
       (next-buffer)
       (message "Visual line mode."))
      (visual-line-mode
       (visual-fill-column-mode -1)
       (visual-line-mode -1)
	  (auto-fill-mode)
	  (message "Auto-fill mode."))
      ((bound-and-true-p auto-fill-function)
       (auto-fill-mode -1)
       (visual-fill-column-mode)
       (message "Visual fill column mode."))
      (t
       (visual-fill-column-mode)
       (message "Visual fill column mode."))))
    (`4
     (cond
      ((bound-and-true-p whitespace-newline-mode)
       (whitespace-newline-mode -1))
      (t
       (whitespace-newline-mode))))
    (`16
     (toggle-truncate-lines))))


;;;; Efficiently Kill buffers and their windows
;; From https://www.emacswiki.org/emacs/download/misc-cmds.el
;; Candidate as a replacement for `kill-buffer', at least when used interactively.
;; For example: (define-key global-map [remap kill-buffer] 'kill-buffer-and-its-windows)
;;
;; We cannot just redefine `kill-buffer', because some programs count on a
;; specific other buffer taking the place of the killed buffer (in the window).
;;;###autoload
(defun vic-dispquirks-kill-buffer-and-its-windows (buffer)
  "Kill BUFFER and delete its windows.  Default is `current-buffer'.
BUFFER may be either a buffer or its name (a string)."
  (interactive (list (read-buffer "Kill buffer: " (current-buffer) 'existing)))
  (setq buffer  (get-buffer buffer))
  (if (buffer-live-p buffer)            ; Kill live buffer only.
      (let ((wins  (get-buffer-window-list buffer nil t))) ; On all frames.
        (when (and (buffer-modified-p buffer)
                   (fboundp '1on1-flash-ding-minibuffer-frame))
          (1on1-flash-ding-minibuffer-frame t)) ; Defined in `oneonone.el'.
        (when (kill-buffer buffer)      ; Only delete windows if buffer killed.
          (dolist (win  wins)           ; (User might keep buffer if modified.)
            (when (window-live-p win)
              ;; Ignore error, in particular,
              ;; "Attempt to delete the sole visible or iconified frame".
              (condition-case nil (delete-window win) (error nil))))))
    (when (interactive-p)
      (error "Cannot kill buffer.  Not a live buffer: `%s'" buffer))))


;;; Protect some buffers from being accidentally killed. That is
;;; useful mainly to avoid killing *scratch* and *Messages*, with deep
;;; regret feelings later on.
(defvar vic-dispquirks-not-to-kill-buffer-list nil "Buffers which should not be killed by normal human means")

;;;###autoload
(defun vic-dispquirks-kill-buffer-but-not-some ()
  (interactive)
  (if (member (buffer-name (current-buffer)) vic-dispquirks-not-to-kill-buffer-list)
      (bury-buffer)
    (kill-buffer (current-buffer))))

(provide 'vic-display-quirks)
