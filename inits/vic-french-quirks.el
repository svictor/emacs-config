(defvar vic-frquirks-spaced-punctuation '("?" "!" ";" ":")
  "Punctuation that should have non-breaking space added. Used in French prose.")

(defun vic-frquirks-space-insert (&rest n)
  "Insert non-break space before ‘vic-spaced-punctuation’.
Useful as advice to ‘self-insert-command’ for some French punctuation."
  (when (member (string (preceding-char)) vic-frquirks-spaced-punctuation)
    (let ((pc (string (char-after (- (point) 2)))))
      (unless (or (member pc vic-frquirks-spaced-punctuation) ;; skip for ?? ?! etc.
		  ;; also skip if previous char was space.
		  ;; This enables entering :: and org-mode :tags:
		  (equal pc " "))
	(backward-char)
	(insert " ")
	(forward-char)))))

(defun vic-frquirks-space-delete ()
  "When deleting char in ‘vic-frquirks-spaced-punctuation’, also delete non-break space before it."
  (when (eq (preceding-char) 160)
    (backward-delete-char 1)))

(defvar-local vic-frquirks-delete-backward-char-hook nil
  "Hook to run after delete-backward-char. Hook can be made buffer-local.")

(defun vic-frquirks-delete-backward-char-hook (&rest n)
  (when vic-frquirks-delete-backward-char-hook
    (mapc 'funcall vic-frquirks-delete-backward-char-hook)))
(advice-add 'delete-backward-char :after 'vic-frquirks-delete-backward-char-hook)

;;;###autoload
(defun vic-frquirks-guillemets ()
  "Insert a pair of guillemets and leave cursor in the middle. 
   If region is active wrap in guillemets instead"
  (interactive)
  ;; store region beginning/end before goto-char. No matter if nil.
  (let ((rbeg (region-beginning))
	(rend (region-end)))
  (if (region-active-p)
      (progn
	(goto-char rend)
	(insert " »")
	(goto-char rbeg)
	(insert "« "))
    (progn
      (insert "«  »");<-- deux espaces insécables
      (backward-char 2)))))

;;;###autoload
(defun vic-frquirks-tirets ()
  "Insert a pair of tirets − and leave cursor in the middle. 
   If region is active wrap in guillemets instead"
  (interactive)
  (let ((rend (region-end))
	(rbeg (region-beginning))
	;; Use space punctuation maybe
	(sp (if vic-frquirks-use-spaced-punctuation "_" "")))
  (if (region-active-p)
      (progn
	(goto-char rend)
	(insert (concat sp "−"))
	(goto-char rbeg)
	(insert (concat "−" sp)))
    (progn
      (insert (concat "−" sp sp "−"))
      (backward-char (+ 1 (length sp)))))))

(defun vic-frquirks-initialize-text-buffer (&optional arg)
  "Initialize the language quirks in the buffer. See ‘vic-frquirks-lang-toggles‘ for possible values of ARG."
  (let ((arg (or arg 1)))
    (apply-partially #'vic-frquirks-lang-toggles arg)))

;;;###autoload
(defun vic-frquirks-lang-toggles (arg)
  "Toggle various things related to language support.
With no C-u, toggle spaced punctuation. With C-u C-u toggle flyspell-mode. With C-u C-u C-u spell-check buffer or region."
  (interactive "p")
  (pcase arg
    (`1 (if (member 'vic-frquirks-space-insert post-self-insert-hook)
	    (progn 
	      (remove-hook 'post-self-insert-hook #'vic-frquirks-space-insert t)
	      (remove-hook 'vic-frquirks-delete-backward-char-hook #'vic-frquirks-space-delete t)
	      (message "Auto-space punctuation: off."))
	  (progn
	    (add-hook (make-local-variable 'post-self-insert-hook) #'vic-frquirks-space-insert 0 t)
	    (add-hook 'vic-frquirks-delete-backward-char-hook #'vic-frquirks-space-delete 0 t)
	    (message "Auto-space punctuation: on."))))
    (`4 (if flyspell-mode
	    (flyspell-mode -1)
	  (flyspell-mode 1))
	(message "Flyspell mode: %s" flyspell-mode))
    (`16 (unless flyspell-mode (flyspell-mode 1))
	 (if (use-region-p)
	     (flyspell-region (region-beginning) (region-end))
	   (flyspell-buffer)))))

;; (add-hook 'text-mode-hook (lambda ()
;; 			    (local-set-key (kbd "C-c ð") #'vic-frquirks-lang-toggles)
;; 			    (apply-partially #'vic-frquirks-lang-toggles 1)))

(provide 'vic-french-quirks)


