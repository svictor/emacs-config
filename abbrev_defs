;;-*-coding: utf-8;-*-
(define-abbrev-table 'lisp-mode-abbrev-table
  '(
    ("vpak" "" vic/require-git-package :count 0)
   ))

(define-abbrev-table 'mail-abbrevs
  '(
    ("katell" "Katell Morand <katell.morand@parisnanterre.fr>" mail-abbrev-expand-hook :count 4)
   ))

(define-abbrev-table 'org-mode-abbrev-table
  '(
    ("cmtr" "[commentaire]" vic/abbrev-make-org-mm-link :count 0)
    ("frd" "[fredonne]" vic/abbrev-make-org-mm-link :count 40)
    ("jjj" "[joue]" vic/abbrev-make-org-mm-link :count 9)
    ("lytrans" "" vic/modal-seances-transcription :count 2)
    ("rvlfrg" "#+ATTR_REVEAL: :frag t" nil :count 2)
   ))

(define-abbrev-table 'php-mode-abbrev-table
  '(
    ("vpp" "" vic-pretty-printr :count 0)
   ))

(define-abbrev-table 'text-mode-abbrev-table
  '(
    ("4c" "tétracorde" nil :count 33)
    ("5c" "pentacorde" nil :count 26)
    ("Hln" "Héléna" nil :count 4)
    ("Orn" "Ourania" nil :count 25)
    ("Sigcnrs" "--

		    Chargé de recherche CNRS

		    Directeur du centre de recherche en ethnomusicologie (CREM-LESC UMR7186)

		    LESC - UMR 7186 - Université Paris Nanterre

		    http://svictor.net | 06 51 21 35 76
" nil :count 0)
    ("Sigcrem" "--

	       	    Centre de recherche en ethnomusicologie
CREM-LESC, UMR 7186, CNRS UPN

		    Université Paris Nanterre

		    Maison Max Weber (bureau 116)

		    + 33 (0)1 46 69 26 66 | +33 (0)6 51 21 35 76

		    https://lesc-cnrs.fr/laboratoire/lesc-crem" nil :count 0)
    ("Srn" "Syriane" nil :count 8)
    ("VAS" "Victor A. Stoichiţă" nil :count 5)
    ("VS" "Victor Stoichiţă" nil :count 5)
    ("afaik" "as far as I know" nil :count 0)
    ("bcp" "beaucoup" nil :count 13)
    ("bcr" "♮" nil :count 3)
    ("bcs" "because" nil :count 1)
    ("bml" "♭" nil :count 3)
    ("btw" "by the way" nil :count 2)
    ("cad" "c'est-à-dire" nil :count 7)
    ("carr" "⮔" nil :count 0)
    ("dac" "d'un autre côté" nil :count 0)
    ("ims" "il me semble que" nil :count 0)
    ("larr" "←" nil :count 1)
    ("lrarr" "⮂" nil :count 0)
    ("otoh" "on the other hand" nil :count 2)
    ("ptet" "peut-être" nil :count 47)
    ("qd" "quand" nil :count 3)
    ("qm" "quand même" nil :count 4)
    ("qq" "quelque" nil :count 3)
    ("qqc" "quelque chose" nil :count 29)
    ("qqn" "quelqu'un" nil :count 7)
    ("qqp" "quelque part" nil :count 0)
    ("qqs" "quelques" nil :count 14)
    ("rarr" "→" nil :count 63)
    ("rarrr" "⇉" nil :count 0)
    ("stp" "s'il te plaît" nil :count 1)
    ("thup" "👍" nil :count 0)
    ("tt" "tout" nil :count 1)
    ("ttf" "tout à fait" nil :count 6)
    ("àma" "à mon avis" nil :count 0)
   ))

